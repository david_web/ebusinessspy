<?php 

namespace App\Contracts;

interface FavoriteInterface
{
	/**
	* Create favorite.
	*
	* @param array $dataArray
	* @return  $favorite
	*/
	public function createOne($dataArray);

	/**
    * Delete favorite by id
    *
    * @param integer $id
    * @return delete
    */
	public function delete($id);
}