<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdvertisingController extends BaseController
{

    protected $fb;
    protected $fbs;
    protected $fb_login_config;
    
    public function __construct()
    {           
        $this->fb_login_config = array('canvas' => 1,'fbconnect' => 1, 'scope' =>'');
        $fb_config = \FacebookVideo::getFBConfig();  
        $this->fb = new \Facebook($fb_config);
        $this->fbs = new \fbsearch();

    }

    /**
    * Get Facebook Videos.
    * GET  /fb-videos
    *
    * @return view
    */
    public function getFbVideos()
    {
        $user = $this->fb->getUser();
        $viewData = array();
        
        if($user){
            $data = [
                'advertising' => true,
                'facebook' => true,
                'fbVideos' => true
            ];
            return view('advertising.facebook.facebook-videos', $data);
        } else {
            $viewData['login_url'] = $this->fb->getLoginUrl($this->fb_login_config);
            $viewData['fb_user']='';
            return view('advertising.facebook.default', array('viewData'=>$viewData,
                                                              'advertising' => true,
                                                              'facebook' => true,
                                                              'fbVideos' => true));
        }   
    }

    /**
    * Get Facebook Videos.
    * GET  /fb-videos-search
    *
    * @return view
    */
    public function getFbVideosSearch($post_type)
    {

            $final = [];
            $page_id = '';
            $this->fbs->init($this->fb->getAccessToken());  
            $output = $this->fbs->search_facebook($post_type);
            // $output = $this->fbs->get_top_posts($page_id, @$post_type);
            $output = $output['data'];          

                // $token='CAALWWhYjQfYBACjMohsuBIFNZBtGZBhA4m69qXqVKyRiJ7BnwF5HmVbSXSe9I2ZAwq3L8NK6FGKDlqIdB2QZBXnZAtxwF99wOW10izwZAnZCKTt4tmi6n6KPWdUP1P4LGFrk2oOOSOZAWwKsp0NZCO4uRX1ZCZCVJPRVJ0C4X1olWbDvQfD8mmCHRTBr3JemJBd5zUZD';          
                // $this->fbs->init($token);
                // $output = $this->fbs->get_top_posts($page_id, @$post_type);
                // $output = @$output['data'];
            
            // Transverce the array and execute
            // foreach($output as $index=>$output_data){
                
            //     $output_data['message'] = preg_replace('/(?<!\S)#([0-9a-zA-Z]+)/m', '<a href="http://anonym.to/?https://www.facebook.com/hashtag/$1?source=feed_text" target="_blank">#$1</a>',@$output_data['message']);
            //     $final[$output_data['likes']['summary']['total_count']] = [
            //         'link'=>@$output_data['link'],
            //         'source'=>@$output_data['source'],
            //         'picture'=>@$output_data['picture'],
            //         'shares'=>@$output_data['shares']['count'],
            //         'likes'=>@$output_data['likes']['summary']['total_count'],
            //         'comments'=>@$output_data['comments']['summary']['total_count'],
            //         'message'=>@$output_data['message'],
            //         'id'=>@$output_data['id']
            //     ];
            // }       
            // krsort($final);
            // //format for Json
            // $json_array = [];
            // foreach($final as $like=>$story_data){
            //     $json_array[]=$story_data;      
            // }

            // $response = ['data' => $json_array];
            return  response()->json($output);



            // $user = $this->fb->getUser();
            // $viewData = array();
            // $formData['q'] = $query;
            // $str = '';
            
            // if ($user) {
                
            //     $viewData['logout_url'] = $this->fb->getLogoutUrl();
                
            //     if(!empty($query)) {
            //         $this->fbs->init($this->fb->getAccessToken());
            //         $output = $this->fbs->search_facebook($query);
            //         $output = $output['data'];
            //         //usort($output, 'sortByOrder');
            //         dd($output[0]['likes']);
            //     }  
                
            //     $viewData['output']=$str;
            //     $viewData['fb_user']=$user;
            // } else {
            //     $viewData['login_url'] = $this->fb->getLoginUrl($this->fb_login_config);
            //     $viewData['fb_user']='';
            // }       

            // return view('facebook.facebook-videos',array('viewData'=>$viewData));
    }

    public function getFbAds()
    {
        $data = [
            'advertising' => true,
            'facebook' => true,
            'fbSearch' => true
        ];
        return view('advertising.facebook.facebook-ads', $data);
    }

    public function getFbTopSearch(Request $request)
    {
        $request = $request->all();
        $page_id = $request['page_id'];
        $post_type = $request['post_type'];
        $this->fbs->init($this->fb->getAccessToken());  
        $output = $this->fbs->get_top_posts($page_id, $post_type);
        $output = $output['data'];
        // dd($output);
        return response()->json($output);
    }

    /**
    * Get Hot Youtube.
    * GET  /hot-youtube
    *
    * @return view
    */
    public function getHotYoutube()
    {
        $data = [
            'advertising' => true,
            'youtube' => true,
            'youtubeHot' => true
        ];
        return view('advertising.youtube.youtube-videos', $data);
    }

    /**
    * Get Youtube Video Search.
    * GET  /youtube-video-search
    *
    * @return view
    */
    public function getYoutubeVideoSearch($min_views = 0, $common_vid = 'no', $query = 'most watched', $page='start')
    {
        $output = \YouTube::searchEx($min_views, $common_vid, $query, $page);
        return response()->json($output);
    }

    /**
    * Get Hot Vimeo.
    * GET  /hot-vimeo
    *
    * @return view
    */
    public function getYoutubeMostPopular()
    {
        $data = [
            'advertising' => true,
            'youtube' => true,
            'youtubeMostPopular' => true
        ];
        return view('advertising.youtube.youtube-most-popular', $data);
    }

    public function getYoutubeMostPopularData()
    {
        $data = \YouTube::searchMostPopular();
        return response()->json($data);
    }

    public function getHotVimeo()
    {
        $data = [
            'advertising' => true,
            'vimeo' => true
        ];
        return view('advertising.vimeo.vimeo-videos', $data);
    }

    /**
    * Get Vimeo Video Search.
    * GET  /vimeo-video-search
    *
    * @return view
    */
    public function getVimeoVideoSearch($query = 'most watched', $page = 1)
    {
        return \Vimeo::searchEx($query, $page);
    }

    /**
    * Get Hot Daily.
    * GET  /hot-daily-motion
    *
    * @return view
    */
    public function getHotDaily()
    {
        $data = [
            'advertising' => true,
            'dailyMotion' => true
        ];
        return view('advertising.daily.daily-videos', $data);
    }

    /**
    * Get Daily Motion Video Search.
    * GET  /daily-motion-video-search
    *
    * @return view
    */
    public function getDailyMotionVideoSearch($query = 'most', $page = 1)
    {
        $daily = new \Dailymotion;
        $args = [
            'search' => $query,
            'page' => $page,
            'limit' => 12,
            'fields'=> ['id', 'url', 'title', 'channel', 'description', 'thumbnail_url', 'duration', 'views_total']
        ];

        $res = $daily->get('/videos', $args);

        $data = [];
        if(isset($res['list'])){
            foreach ($res['list'] as $v) {

                $data[] = [
                    "id" => $v['id'],
                    "url" => $v['url'],
                    "title" => str_replace( "'","",$v['title']),
                    "channel" => $v['channel'],
                    "description" => strip_tags(str_replace( "'","",$v['description'])),
                    "thumbnail_url" => $v['thumbnail_url'],
                    "duration" => \YouTube::yt_duration($v['duration']),
                    "views_total" => number_format($v['views_total'])
                ];
            }
        }
        return response()->json($data);
    }
}
