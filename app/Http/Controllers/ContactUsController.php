<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Auth;

class ContactUsController extends BaseController
{
    public function main(Request $request)
	{
		$requestData = $request->all();
		$data = [			
				'contact_type'=> $requestData['contact_type'],
				'contact_subject'=> $requestData['contact_subject'],
				'contact_message'=> $requestData['contact_message'],	
				'contact_name'=> Auth::user()->name,
				'contact_email'=> Auth::user()->email				
			];		
	
		// Session::put('contact_type',$data['contact_type']);
		// Session::put('contact_subject',$data['contact_subject']);
	
		Mail::send('email-contact-us', $data, function($message) use ($data){
			$message->from($data['contact_email'] ,$data['contact_name']);
		    $message->to('support@ebusinessspy.com', 'Support')->subject( $data['contact_type'].': '. $data['contact_subject']);
		});
		
		$success = false; $details = '';
		if (count(Mail::failures()) > 0) {
			$details = 'Failed to send message, please try again.';		    
		} else {
			$success = true;
			$details = "Thank you for contacting us. We will get back to you soon.";
		}

		$successData = [
				'success' => $success,
				'details' => $details
			];
		return  response()->json($successData);
	}
}
