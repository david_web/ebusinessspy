<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\SearchKey;
use Auth;
use Exception;
use App\Contracts\FavoriteInterface;

class FavoritesController extends BaseController
{
    public function getFavorites()
    {
        $data = [
            'favorites' => true
        ];
    	return view('favorites', $data);
    }

    /**
    * Get favorite items.
    * GET  /favorite-items
    *
    * @param FavoriteInterface $favoriteRepo
    * @return json
    */
    public function getFavoriteItems(FavoriteInterface $favoriteRepo)
    {
        $userId = Auth::user()->id;
    	$shops = Shop::with([
            'searchKey' => function($query) use ($userId) {
                $query->where('user_id', $userId);
            },
            'favoriteItems' => function($query) use ($userId) {
                $query->where('user_id', $userId);
            }
        ])->get();

    	$data = [
    		'shops' => $shops
    	];
    	return response()->json($data);
    }

    /**
    * Add favorite item.
    * POST  /add-favorite-items
    *
    * @param Request $request
    * @param FavoriteInterface $favoriteRepo
    * @return json
    */
    public function postAddFavoriteItem(Request $request, FavoriteInterface $favoriteRepo)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $favoriteRepo->createOne($data);
        return response()->json(['result' => 'success']);
    }

    /**
    * Delete favorite item.
    * GET  /favorites/delete-favorite-item
    *
    * @param integer $id
    * @param FavoriteInterface $favoriteRepo
    * @return json
    */
    public function getDeleteFavoriteItem($id, FavoriteInterface $favoriteRepo)
    {
        try {
            $favoriteRepo->delete($id);
        } catch (Exception $e) {
            return response()->json(['result' => 'error']);
        }
        return response()->json(['result' => 'success']);
    }

    /**
    * Add search key.
    * GET  /favorites/search-key
    *
    * @return view
    */
    public function getSearchKey($keyWord, $shopId)
    {
        $userId = Auth::user()->id;
        $data = [
            'user_id' => $userId,
            'key_word' => $keyWord,
            'shop_id' => $shopId
        ];
        $searchKey = SearchKey::create($data);
        return response()->json(['searchKey' => $searchKey]);
    }

    /**
    * Get search key.
    * GET  /favorites/search-keys
    *
    * @return view
    */
    public function getSearchKeys()
    {
        $searchKeys = Auth::user()->searchKeys;

        return response()->json(['searchKeys' => $searchKeys]);
    }

    /**
    * Delete search key.
    * GET  /favorites/delete-search-key
    *
    * @return view
    */
    public function deleteSearchKey($id)
    {
        try {
            SearchKey::destroy($id);
        } catch (Exception $e) {
            return response()->json(['result' => 'error']);
        }
        return response()->json(['result' => 'success']);
    }
}
