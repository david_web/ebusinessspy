<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends BaseController
{
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        return view('dashboard');
    }
}
