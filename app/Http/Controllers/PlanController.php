<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Spark\User as SparkUser;
use Laravel\Spark\Subscription;
use Laravel\Spark\Spark;
use Laravel\Spark\Contracts\Interactions\Subscribe;
use Auth;

class PlanController extends BaseController
{
    /**
     * Create subscription jVzoon .
     *
     * @param  Request  $request
     * @return Response
     */
    public function addSubscription(Request $request)
    {
        $requestData = $request->all();
        $user = SparkUser::where('id', 1)->first();
        // $userId = Auth::user()->id;
        $stripeId = $requestData['caffitid'];
        $stripePlan = $requestData['cprodtitle'];
        $subscription = new Subscription;
        $data = [
            'user_id' => 1,
            'stripe_id' => $stripeId,
            'stripe_plan' => $stripePlan,
            'quantity' => 1,
            'name' => 'default'
        ];

        $subscription->create($data);
        $user->update(['stripe_id' => $stripeId]);
        // $data = [
        //     "stripe_token" => "tok_19rpUdKrokuRgvBcIrHqrR6h",
        //     "plan" => $stripePlan,
        //     "coupon" => null,
        //     "address" => null,
        //     "address_line_2" => null,
        //     "city" => null,
        //     "state" => null,
        //     "zip" => "0069",
        //     "country" => "US",
        //     "vat_id" => null,
        //     "errors" => [
        //     "errors" => [],
        //     ],
        //     "busy" => true,
        //     "successful" => false
        // ];
        // $plan = Spark::plans()->where('id', 'monthly-pro')->first();
        // $aaa = Spark::interact(Subscribe::class, [
        //     $user, $plan, false, $data
        // ]);
    }

    public function cancellSubscription()
    {
    	$user = Auth::user();
    	$subscription = new Subscription;
    	$stripeId = $user->stripe_id;
    	$user->update(['stripe_id' => null]);
    	$subscription->where('stripe_id', $stripeId)->delete();
    }
}
