<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Inflect;
use DB;

class ShopifyController extends BaseController
{
    /**
    * Get Shopify Best Sellers.
    * GET  /shopify-best-sellers
    *
    * @return view
    */
    public function getShopifyBestSellers()
    {
        $data = [
            'shopifyBestSellers' => true
        ];
        return view('advertising.shopify.shopify', $data);
    }

    /**
    * Get Shopify Best Sellers Search.
    * GET  /shopify-best-sellers-search
    *
    * @return view
    */
    public function getShopifyBestSellersSearch(Request $request)
    {
        mb_internal_encoding('UTF-8');
        
        $aColumns = array( 'website', 'global_rank', 'traffic_volume', 'id' );
          
        // Indexed column (used for fast and accurate table cardinality)
        $sIndexColumn = 'id';
          
        // DB table to use
        $sTable = 'websites';   
                        
        $input = $request->all();
          
        /**
         * Paging
         */
        $sLimit = "";
        if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
            $sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
        }
          
          
        /**
         * Ordering
         */
        $aOrderingRules = [];
        if ( isset( $input['iSortCol_0'] ) ) {
            $iSortingCols = intval( $input['iSortingCols'] );
            for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
                if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
                    $aOrderingRules[] =
                        " ".$aColumns[ intval( $input['iSortCol_'.$i] ) ]." "
                        .($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
                }
            }
        }
         
        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY traffic_volume DESC";
        }

        $iColumnCount = count($aColumns);       
        
        $searchableColumns = ['website_tags', 'website_organic_keywords'];

        $aFilteringRules = [];
        //check if there's a space
        if(isset($input['sSearch'])){
            $_searchInput = strtolower($input['sSearch']);
        }else{
            $_searchInput = '';
            return "Invalid Access";
        }
                
        if( preg_match('/\s/',$_searchInput) ){
            $_searchEx = explode(' ', $_searchInput);
            // Individual column filtering (multi words)
            for($j=0; $j<count($_searchEx); $j++){

                $_q = Inflect::singularize($_searchEx[$j]);
                
                $aFilteringRules[] = ' LOWER(website_organic_keywords) LIKE "%'. $_q .'%" AND LOWER(website_tags) LIKE "%'. $_q .'%"';
                $aFilteringRules[] = ' LOWER(website_organic_keywords) LIKE "%'. $_q .'%" AND LOWER(website_paid_keywords) LIKE "%'. $_q .'%"';
                $aFilteringRules[] = ' LOWER(website_tags) LIKE "%'. $_q .'%" AND LOWER(website_paid_keywords) LIKE "%'. $_q .'%"';
                
            }
                
        }else{      
                
            $_q = Inflect::singularize($_searchInput);

            $aFilteringRules[] = ' LOWER(website_organic_keywords) LIKE "%'. $_q .'%" AND LOWER(website_tags) LIKE "%'. $_q .'%"';
            $aFilteringRules[] = ' LOWER(website_organic_keywords) LIKE "%'. $_q .'%" AND LOWER(website_paid_keywords) LIKE "%'. $_q .'%"';
            $aFilteringRules[] = ' LOWER(website_tags) LIKE "%'. $_q .'%" AND LOWER(website_paid_keywords) LIKE "%'. $_q .'%"';

        }       
        
        if (!empty($aFilteringRules)) {         
            $sWhere = ' WHERE id>0 AND  website LIKE "%'.$_searchInput.'%" OR ('.implode(' OR ', $aFilteringRules).') ';
        } else {
            $sWhere = ' WHERE id>0 ';
        }   

        /**
         * SQL queries
         * Get data to display
         */
        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
         
        $sQuery = '
            SELECT SQL_CALC_FOUND_ROWS 
                '.implode(', ', $aQueryColumns).'
            FROM '.$sTable.' '.$sWhere.$sOrder.$sLimit;          
        
        $rResult = DB::select(DB::raw( $sQuery ));    

        // Data set length after filtering
        $sQuery = "SELECT FOUND_ROWS() as total";
        $rResultFilterTotal = DB::select(DB::raw( $sQuery ));   
        $iFilteredTotal = $rResultFilterTotal[0]->total;
        
        // Total data set length
        $sQuery = "SELECT COUNT(`".$sIndexColumn."`) as total FROM `".$sTable."`";
        $rResultFilterTotal = DB::select(DB::raw( $sQuery )); 
        $iTotal = $rResultFilterTotal[0]->total;

        /**
         * Output
         */
        $sEcho = '';
        if(isset($input['sEcho'])){
            $sEcho = intval($input['sEcho']);
        }

        $output = array(
            "sEcho"                => $sEcho,
            "iTotalRecords"        => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData"               => array(),
        );

        foreach ($rResult as $res) { 
            //format display
            $data = array();
            
            $global_rank = str_replace(',', '', $res->global_rank);

            $traffic_volume = str_replace(',', '', $res->traffic_volume);
            // $global_traffic = $res->global_traffic;
            $best_seller = "https://".$res->website."/collections/all?sort_by=best-selling";
            $traffic_stats = "https://www.similarweb.com/website/".$res->website;
            
            $data['webSite'] = $res->website; //website
            $data['globalRank'] = ($global_rank != null && $global_rank != 0) ? number_format($global_rank) : "-"; //global_rank
            // $data[2] = number_format($global_traffic,4); //global  traffic
            $data['trafficVolume'] = ($traffic_volume != null && $traffic_volume != 0) ? number_format($traffic_volume) : "-"; //traffic volume
            $data['bestSellers'] = $best_seller; //best sellers

            $data['trafficStats'] = $traffic_stats;

            $output['aaData'][] = $data;
        }

        return response()->json($output);
    }
}
