<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\SearchKey;
use Auth;

class ShopsController extends BaseController
{
    /**
    * Get Ebay products.
    * GET  /ebay
    *
    * @return view
    */
    public function getEbay()
    {
        $data = [
            'ecommerce' => true,
            'ebay' => true
        ];
        return view('shops.ebay', $data);
    }

    /**
    * Get Ebay products.
    * GET  /ebay-search
    *
    * @return json
    */
    public function getEbaySearch($query = 'undefined',
                                  $categoryID = 'none',
                                  $sort = 'none',
                                  $order = 'none',
                                  $page = 1)
    {
        $mostPopularEbay = \EbayWatchCount::searchEx($query, $categoryID, $sort, $order, $page);
        $data = [
            'mostPopularEbay' => $mostPopularEbay
        ];
        return response()->json($data);
    }

    /**
    * Get AliExpress products.
    * GET  /ali-express
    *
    * @return view
    */
    public function getAliExpress()
    {
        $data = [
            'ecommerce' => true,
            'aliExpress' => true
        ];
        return view('shops.ali-express', $data);
    }

    /**
    * Get AliExpress products.
    * GET  /ali-express-search
    *
    * @return json
    */
    public function getAliExpressSearch($query, $category, $sort, $page = 1)
    {
    	$aliExpressProducts = \AliExpressAPI::search($category, $sort, $query, $page);
        $data = [
            'aliExpressProducts' => $aliExpressProducts
        ];
        return response()->json($data);
    }

    /**
    * Get Amazon products page.
    * GET  /amazon
    *
    * @return view
    */
    public function getAmazon()
    {
        $data = [
            'ecommerce' => true,
            'amazon' => true
        ];
        return view('shops.amazon', $data);
    }

    /**
    * Get Amazon products.
    * GET  /amazon-search
    *
    * @return json
    */
    public function getAmazonSearch($query, $page)
    {
        $amazonProducts = \Amazon::searchEx($query, $page);
        $data = [
            'amazonProducts' => $amazonProducts
        ];
        return response()->json($data);
    }

    /**
    * Get Wanelo products page.
    * GET  /wanelo
    *
    * @return view
    */
    public function getWanelo()
    {
        $data = [
            'ecommerce' => true,
            'wanelo' => true
        ];
        return view('shops.wanelo', $data);
    }

    /**
    * Get Wanelo products.
    * GET  /wanelo-search
    *
    * @return json
    */
    public function getWaneloSearch($category, $sort, $query, $page = 1)
    {
        $waneloProducts = \Wanelo::searchEx($category, $sort ,$query, $page);
        $data = [
            'waneloProducts' => $waneloProducts
        ];
        return response()->json($data);
    }

    /**
    * Get Wanelo products.
    * GET  /wanelo
    *
    * @return view
    */
    public function getEtsy()
    {
        $data = [
            'ecommerce' => true,
            'etsy' => true
        ];
        return view('shops.etsy', $data);
    }

    /**
    * Get Wanelo products.
    * GET  /wanelo
    *
    * @return view
    */
    public function getEtsySearch($category, $query, $sort, $page = 1)
    {
        $etsyProducts = \Etsy::searchEx($category, $query ,$sort, $page);
        $data = [
            'etsyProducts' => $etsyProducts
        ];
        return response()->json($data);
    }
}
