<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TshirtController extends BaseController
{
    public function getTshirtResearch()
    {
        $data = [
            'tShirt' => true,
            'tShirtSpy' => true
        ];
    	return view('t-shirt.tshirt-research', $data);
    }

    public function getSkreened($query, $page)
    {
    	return \Skreened::search($query, $page);
    }

    public function getZazzle($query, $page)
    {
    	return \Zazzle::searchEx($query, $page);
    }

    public function getCafepress($query, $page)
    {
    	return \Cafepress::search($query, $page);
    }

    public function getSunFrog($query, $page)
    {
    	return \SunFrog::searchEx($query, $page);
    }

    public function getTeeSpring($query, $page)
    {
    	return \TeeSpring::searchEx($query, $page);
    }

    public function getImages($query, $page)
    {
    	return \Google::searchImages($query, $page);
    }

    public function getSunFrogBestPage()
    {
        $data = [
            'tShirt' => true,
            'sunFrog' => true
        ];
		return view('t-shirt.sunfrog-best-sellers', $data);
    }

    public function getSunFrogBest($category = 'all')
    {
        $category = strtolower($category);
        $category = str_replace(" ","-",$category);
		return \SunFrogBestSellers::searchEx($category);
    }

    public function getTeeSpringTrends()
    {
        $data = [
            'tShirt' => true,
            'teeSpring' => true
        ];
        return view('t-shirt.tee-spring-trends', $data);
    }

    public function getBestTeeSpringTrends($category = 'all')
    {
        $data = \TeeShirtTrending::searchEx($category);
        foreach ($data as $key => $value) {
            $data[$key]['link'] = 'https://teespring.com'.$data[$key]['link'];
        }
        return $data;
    }
}
