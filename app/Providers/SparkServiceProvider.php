<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Your Company',
        'product' => 'ebusinessspy',
        'street' => 'PO Box 111',
        'location' => 'Your Town, NY 12345',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'mher2099@gmail.com',
        'david1994martirosyan@gmail.com',
        'a@mail.ru'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Spark::useStripe()->noCardUpFront()->trialDays(10);

        Spark::freePlan()
            ->features([
                'First', 'Second', 'Third'
            ]);

        // Spark::plan('Basic', 'provider-id-1')
        //     ->price(10)
        //     ->features([
        //         'First', 'Second', 'Third'
        //     ]);

        Spark::plan('Pro', 'monthly-pro')
        ->price(20)
        ->features([
            'Feature 1',
            'Feature 2',
            'Feature 3',
        ]);

        Spark::plan('Pro', 'yearly-pro')
        ->price(100)
        ->yearly()
        ->features([
            'Feature 1',
            'Feature 2',
            'Feature 3',
        ]);
    }
}
