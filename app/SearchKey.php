<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchKey extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'key_word',
        'shop_id'
    ];

     /**
     * Get the phone record associated with the user.
     */
    public function shop()
    {
        return $this->hasOne('App\Shop');
    }
}
