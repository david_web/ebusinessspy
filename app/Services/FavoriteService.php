<?php 

namespace App\Services;

use App\Contracts\FavoriteInterface;
use App\Favorite;

class FavoriteService implements FavoriteInterface
{
	/**
	* Create a new instance of FavoriteService class
	*
	* @return void
	*/
	public function __construct()
	{
		$this->favorite = new Favorite();
	}

	/**
	* Create favorite.
	*
	* @param array $dataArray
	* @return  $favorite
	*/
	public function createOne($dataArray)
	{
		return $this->favorite->create($dataArray);
	}

	/**
    * Delete favorite by id
    *
    * @param integer $id
    * @return delete
    */
	public function delete($id)
	{
		return $this->favorite->find($id)->delete();
	}
}