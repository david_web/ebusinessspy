<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    /**
    * Get the searchKeys record associated with the shop.
    */
    public function searchKey()
    {
        return $this->hasMany('App\SearchKey', 'shop_id', 'id');
    }

    /**
    * Get the searchKeys record associated with the shop.
    */
    public function favoriteItems()
    {
        return $this->hasMany('App\Favorite', 'shop_id', 'id');
    }
}
