<?php


class AliExpressAPI {

	public static function search($category, $sorting, $query, $page=1)
	{
		// http://portals.aliexpress.com/help/help_center_API.html
		$fields =  "productId,productTitle,imageUrl,productUrl,originalPrice,salePrice,packageType,evaluateScore";
		
		$categ = '';
		if($category!='all'){
			$categ = '&categoryId='.$category;
		}

		$sort = '';
		if($sorting!='any'){
			$sort = '&sort='.$sorting;
		}

		$url = "http://gw.api.alibaba.com/openapi/param2/2/portals.open/api.listPromotionProduct/51414?fields={$fields}";
		$url .="&keywords={$query}&pageNo={$page}".$categ.$sort;
		
		$res = Scraper::get($url, "json", false);
		$data = array();

		if(isset($res['result']['products']) && count($res['result']['products'])>0)
		{
			foreach ($res['result']['products'] as $key => $row) {
				$ratePercent = ($row['evaluateScore'] / 5) * 100;
				$starTitle = "Star Rating: ".$row['evaluateScore']." out of 5";
				if($ratePercent==0){
					$ratePercent = '';
					$starTitle = '';
				}

				$price = str_replace('US ','',$row['originalPrice']);
				$originalPrice = $price;
				$p = explode('$',$row['originalPrice']);
				if(isset($p[1])){
					$originalPrice = (float)trim($p[1]);
					$price = number_format( $originalPrice,2 );
				}
				
				$salePrice = str_replace('US ','',$row['salePrice']);
				$newPrice = $salePrice;
				$p = explode('$',$row['salePrice']);
				if(isset($p[1])){
					$newPrice = (float)trim($p[1]);
					$salePrice = number_format( $newPrice,2 );
				}

				$data[] = [
					"productId"		=> $row['productId'],
					"productTitle"	=> $row['productTitle'],
					"thumbUrl" 		=> $row['imageUrl'],
					"productUrl"	=> $row['productUrl'],
					"originalPrice" => $originalPrice,
					"price" 		=> $price,
					"salePrice" 	=> $salePrice,
					"newPrice" 		=> $newPrice,
					"packageType"	=> $row['packageType'],
					"evaluateScore"	=> $row['evaluateScore'],
					"starTitle"		=> $starTitle,
					"ratePercent"	=> $ratePercent
				];
			}
		}		

		return $data;
	}

}