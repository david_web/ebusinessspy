<?php

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Finding\Services;
use \DTS\eBaySDK\Finding\Types;
use \DTS\eBaySDK\Finding\Enums;

class Ebay
{
	public function __construct()
	{
		$this->service = new Services\FindingService([
		    'credentials' => config('ebay.production.credentials'),
		    'globalId'    => Constants\GlobalIds::US
		]);
	}

	public static function search($query, $page = 1) {

		// Create the request object.
		$request = new Types\FindItemsByKeywordsRequest();

		$request->outputSelector = ['PictureURLLarge', 'listingInfo'];

		$request->keywords = $query;

		/**
		 * Limit the results to 10 items per page.
		 */
		$request->paginationInput = new Types\PaginationInput();
		$request->paginationInput->entriesPerPage = 10;

		for ($pageNum = 1; $pageNum <= $page; $pageNum++ ) {

			$request->paginationInput->pageNumber = $pageNum;

			// Send the request to the service operation.
			$response = $this->service->findItemsByKeywords($request);

			if ($pageNum == $page) {
				$i = 0;
				foreach ($response->searchResult->item as $item) {

		            $data[$i] = [
		    			"id" 			=> $item->itemId,
						"title" 		=> $item->title,
						"subtitle"		=> $item->subtitle,
						"info"			=> $item->listingInfo,
						"thumb"			=> $item->galleryURL,
						"thumb_large"	=> $item->pictureURLLarge,
						"url"			=> $item->viewItemURL,
						"currency" 		=> $item->sellingStatus->currentPrice->currencyId,
						"price" 		=> $item->sellingStatus->currentPrice->value,
					];
		            $i++;
				}
			}
		}

		return $data;
	}

	public static function searchEx($query, $page = 1) {

		// Create the request object.
		$request = new Types\FindItemsByKeywordsRequest();

		$request->outputSelector = ['PictureURLLarge', 'listingInfo'];

		$request->keywords = $query;

		/**
		 * Limit the results to 10 items per page.
		 */
		$request->paginationInput = new Types\PaginationInput();
		$request->paginationInput->entriesPerPage = 10;

		for ($pageNum = 1; $pageNum <= $page; $pageNum++ ) {

			$request->paginationInput->pageNumber = $pageNum;

			// Send the request to the service operation.
			$response = $this->service->findItemsByKeywords($request);

			if ($pageNum == $page) {
				$i = 0;
				foreach ($response->searchResult->item as $item) {

		            $data[$i] = [
		    			"id" 			=> $item->itemId,
						"title" 		=> $item->title,
						"subtitle"		=> $item->subtitle,
						"info"			=> $item->listingInfo,
						"thumbUrl"		=> $item->galleryURL,
						"url"			=> $item->pictureURLLarge,
						"surl"			=> $item->viewItemURL,
						"currency" 		=> $item->sellingStatus->currentPrice->currencyId,
						"price" 		=> $item->sellingStatus->currentPrice->value,
					];

		            $i++;
				}
			}
		}

		return $data;
	}
}