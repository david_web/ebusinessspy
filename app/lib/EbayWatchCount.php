<?php

class EbayWatchCount {

	protected static $s_endpoint = "http://open.api.ebay.com/shopping?";  // Shopping URL to call
	protected static $cellColor = "bgcolor=\"#dfefff\"";  // Light blue background used for selected items 
	protected static $m_endpoint = 'http://svcs.ebay.com/MerchandisingService?';  // Merchandising URL to call
	protected static $appid = 'MherKul-shoplica-PRD-e2466ad0e-0de7a58c';  // You will need to supply your own AppID
	// protected static $appid = 'JoaoGilV-e88f-4417-9262-abde51e116ea';
	protected static $responseEncoding = 'XML';  // Type of response we want back

	public static function searchEx($query, $categoryID, $sort, $order, $page) {
		return self::getMostWatchedItemsResults($query, $categoryID, $sort, $order, $page, '', '');
	}

	 // Create a function for the getMostWatchedItems call 
	private static function getMostWatchedItemsResults ($query, $categoryID, $_sort, $_order, $page, $selectedItemID = '', $cellColor = '') {
		
		$categIDArr = array();
		$dummyData = array(); 
		$itemIDArr = array();	
		$data = array();
		
	    //GET QUERY
	    //================================
		
	    if( $categoryID!='' && $categoryID!='undefined' ){
	    	$dummyData = self::FindPopularItems( $query, $categoryID, $dummyData, $itemIDArr );
	  //   	echo "<pre>";
			// print_r($dummyData);
			// echo "</pre>";
			// exit();
		}else{
			
			// DEFAULT
		    $getData = self::getMostWatchedItems();
		    $dummyData = self::getPastSales( $getData['dummyData'], $getData['itemIDArr'] );

		} //end DEFAULT
		

		//SORT & ORDER
	    //==============================
	    if(count($dummyData)>0){
	    	$data = self::applySortOrder ($dummyData, $_sort, $_order);
	    } //end of itemIDArr check
		
	    return $data;

	} // End of getMostWatchedItemsResults function

	//===================================================================
	/*
		TODO: FindPopularItems is deprecated, need to change call
		possible option: 
		findItemsAdvanced - (get items) http://developer.ebay.com/DevZone/finding/CallRef/findItemsAdvanced.html#Samples
		GetSingleItem - (get desc) http://developer.ebay.com/DevZone/shopping/docs/CallRef/GetSingleItem.html#Samples

		CURRENT FLOW:
		default: getMostWatchedItems, then getPastSales
		on query: 
			FindPopularItems - get items based on query
				getPastSales - get past sales
				GetMultipleItems - get images for products without images
	*/

	//===================================================================



	private static function FindPopularItems( $query, $categoryID, $dummyData, $itemIDArr ){
		$noImageArr = array();

		$getCategUsingQuery = "http://open.api.ebay.com/shopping?"
							  ."callname=FindPopularItems&"
							  ."responseencoding=XML&"
							  ."appid=".self::$appid."&"
							  ."siteid=0&"
							  ."MaxEntries=75&"
							  ."version=531&"
							  ."QueryKeywords=".$query;

		if($categoryID!='' && $categoryID!='none'){
			$getCategUsingQuery .= "&CategoryID=".$categoryID;
		}

		$resp = simplexml_load_file($getCategUsingQuery);

		if($resp){

			if($resp->Ack=='Success'){
				
			    // For each item node, build a table cell and append it to $retna 
			    $ctr=0; $itemIDArr2 = array(); 
			    foreach($resp->ItemArray->Item as $item) {	

					$itemID = (string)$item->ItemID;

					$thumbUrl = (string)$item->GalleryURL;
					$thumbUrl = str_replace('8080', '4040', $thumbUrl);
					$imageExist = self::checkIfExist( $thumbUrl );
					// $noImageArr[ $itemID ] = $itemID;
					
					if($thumbUrl=='' || !$imageExist ){
						$noImageArr[ $itemID ] = $itemID; 
					}					

					if(!in_array($itemID, $itemIDArr)){

				        $itemIDArr[] = $itemID;	
				        $itemIDArr2[] = $itemID;	

						$dummyData[$itemID] = array(
					    			"id" 			=> $itemID,
									"caption" 		=> (string)$item->Title,
									"thumbUrl" 		=> $thumbUrl,
									"watchCount" 	=> (float)$item->WatchCount,
									"viewItemURL"	=> (string)$item->ViewItemURLForNaturalSearch,
									"timeLeft"		=> self::getPrettyTimeFromEbayTime($item->TimeLeft),										
									"pastSales"     => '0',
									"price" 		=> (float)$item->ConvertedCurrentPrice,
									"description"   => ''
								);

						$ctr++;
						
					}

					// return $dummyData;

					// $obj = $resp->ItemArray->Item ;
					if($ctr==10){							
						$dummyData = self::getPastSales( $dummyData, $itemIDArr2 );

						// return $dummyData;

						//reset arrays
						$itemIDArr2 = array();	
						// $noImageArr = array();	

						$ctr=0;
					}

			    }//end of foreach


			    if(count($noImageArr)>0){
			    	// $dummyData = self::getMissingImages( $dummyData, $noImageArr );
			    	//do another call to get missing images
				    $apicall = "http://open.api.ebay.com/shopping?"
				           ."callname=GetMultipleItems&"
				           ."responseencoding=XML&"
				           ."appid=".self::$appid."&"
				           ."siteid=0&"
				           ."version=525&"
				           // ."IncludeSelector=Details&"
				           ."ItemID=".implode(",",$noImageArr);

				    $resp = simplexml_load_file($apicall);

				    //update pastSales
				    if($resp){
				        if ($resp->Ack == "Success") {            
				            foreach($resp->Item as $item) {
				            	// return $item; exit();
				                $itemID = (string)$item->ItemID;
				                $thumbUrl =(string)$item->pictureURLSuperSize;
				                if($thumbUrl=='') $thumbUrl = (string)$item->PictureURL;
				                $dummyData[$itemID]["thumbUrl"] = $thumbUrl;
				            }                        
				        }
				    }
				}
			    

			    /*if(count($itemIDArr2)>0){
				    //get past sales for extra data not covered by ctr
				    $dummyData = self::getPastSales( $dummyData, $itemIDArr2 );
				}*/

			}
		}


		


		//get more
		//==================================
		//to do

		// if(count($itemIDArr)>0){
		// 	$dummyData = self::getPastSales( $dummyData, $itemIDArr );
		// }
		
		return $dummyData;
	}

	private static function getMostWatchedItems(){

		$dummyData = array(); $itemIDArr = array();
		//show random values
		// $categ = [20081,550,2984,267,12576,625,11450,11116,1,58058,293,14339,11232,45100,26395,11700,281,15032,11233,173484,870,10542,382,64482,260,220,9800,6028,1249,172008,99];
		// $categoryId = $categ[rand(0,30)];

		$apicalla  = self::$m_endpoint;
	    $apicalla .= "OPERATION-NAME=getMostWatchedItems";
	    $apicalla .= "&SERVICE-VERSION=1.0.0";
	    $apicalla .= "&CONSUMER-ID=".self::$appid;
	    $apicalla .= "&RESPONSE-DATA-FORMAT=".self::$responseEncoding;
	    $apicalla .= "&maxResults=5";		    	    

	    $resp = simplexml_load_file($apicalla);
	    
	    // For each item node, build a table cell and append it to $retna 
	    foreach($resp->itemRecommendations->item as $item) {

	    	$itemID = (string)$item->itemId;				        		

    		if(!in_array($itemID, $itemIDArr)){

    			$itemIDArr[] = $itemID;	
		        // Determine which price to display
		        if ($item->currentPrice) {
		        	$price = $item->currentPrice;
		        } else {
		        	$price = $item->buyItNowPrice;
		        }
				
				$dummyData[$itemID] = [
				    			"id" 			=> (string)$item->itemId,
								"caption" 		=> (string)$item->title,
								"thumbUrl" 		=> (string)$item->imageURL,
								"watchCount" 	=> (float)$item->watchCount,
								"viewItemURL"	=> (string)$item->viewItemURL,
								"timeLeft"		=> self::getPrettyTimeFromEbayTime($item->timeLeft),
								"currency"		=> '',
								// "currency" 		=> $item->sellingStatus->currentPrice->currencyId,
								"price" 		=> (float)$price
							];
			}

	    }

	    return array('dummyData'=>$dummyData, 'itemIDArr'=>$itemIDArr);
	}

	private function getMissingImages( $dummyData, $noImageArr){
		//do another call to get PAST SALES
	    $apicall = "http://open.api.ebay.com/shopping?"
	           ."callname=GetMultipleItems&"
	           ."responseencoding=XML&"
	           ."appid=".self::$appid."&"
	           ."siteid=0&"
	           ."version=525&"
	           ."IncludeSelector=Details&"
	           ."ItemID=".implode(",",$noImageArr);

	    $resp = simplexml_load_file($apicall);

	    //update pastSales
	    if($resp){
	        if ($resp->Ack == "Success") {            
	            foreach($resp->Item as $item) {
	            	// return $item; exit();
	                $itemID = (string)$item->ItemID;
	                $thumbUrl =(string)$item->GalleryURL;
	                $dummyData[$itemID]["thumbUrl"] = $thumbUrl;
	            }                        
	        }
	    }

	    return $dummyData;
	}

	private static function getPastSales( $dummyData, $itemIDArr ){
		//do another call to get PAST SALES
	    $apicall = "http://open.api.ebay.com/shopping?"
	           ."callname=GetMultipleItems&"
	           ."responseencoding=XML&"
	           ."appid=".self::$appid."&"
	           ."siteid=0&"
	           ."version=525&"
	           ."IncludeSelector=Details&"
	           ."ItemID=".implode(",",$itemIDArr);

	    $resp = simplexml_load_file($apicall);

	    //update pastSales
	    if($resp){
	        if ($resp->Ack == "Success") {  
	       
	       // return $resp->Item;

	            foreach($resp->Item as $item) {
	                $itemID = (string)$item->ItemID;
	                $pastSales = '0';
	                if( (float)$item->QuantitySold>0 ) $pastSales = (float)$item->QuantitySold;
	                $dummyData[$itemID]["pastSales"] = $pastSales;
	                // $Description = (array)$item->Description;
	                $dummyData[$itemID]["description"] = '';//$Description[0];
	                
	             //    if($item->PictureURL){
		            //     $ImageSet = array();
	             //    	foreach ($item->PictureURL as $k => $url) {	                		
	             //    		$ImageSet[] = $url;
	             //    	}                	
	             //    	$dummyData[$itemID]["ImageSet"] = implode('|',$ImageSet);		                
		            // }
	            }                        
	        }
	    }

	    return $dummyData;
	}	

	private static function applySortOrder ($dummyData, $_sort, $_order) {
		//APPLY SORT
	    //================================
	    if($_sort=='' || $_sort=='none' ){
	    	$_sort = 'watchCount';
		}

		$sortArr = array();
		foreach($dummyData as $k=>$v) {
			
			switch ($_sort) {
				case 'price': $sortArr['price'][$k] = $v['price'];	break;
				case 'pastSales': $sortArr['pastSales'][$k] = $v['pastSales'];	break;
				case 'watchCount': $sortArr['watchCount'][$k] = $v['watchCount'];	break;
				default: $sortArr['watchCount'][$k] = $v['price'];	break;
			}				    
		}

		if($_order!='' && $_order!='none'){
			if($_order=='asc'){
				array_multisort($sortArr[$_sort], SORT_ASC, $dummyData);
			}else{
				array_multisort($sortArr[$_sort], SORT_DESC, $dummyData);
			}
		}else{
			array_multisort($sortArr[$_sort], SORT_DESC, $dummyData);	
		}		

    	//reformat data
	    $i = 0;
	    foreach ($dummyData as $key => $value) {	

			$dummyData[$key]['watchCount'] = number_format( $value['watchCount'] );
			$dummyData[$key]['pastSales'] = !empty($value['pastSales']) ? number_format( $value['pastSales'] ) : '';
	    	$dummyData[$key]['price'] = !empty($value['price']) ? number_format( $value['price'] ) : '';
	    	
	    	$data[$i] = $dummyData[$key];
	    $i++;

	    }

		return $data;
	}

	private static function checkIfExist($url){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,$url);
	    // don't download content
	    curl_setopt($ch, CURLOPT_NOBODY, 1);
	    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(curl_exec($ch)!==FALSE)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}

	public static function getProductDetails( $ItemID ){
		$imageSet = '';

	    $apicall = "http://open.api.ebay.com/shopping?"
	           ."callname=GetSingleItem&"
	           ."responseencoding=XML&"
	           ."appid=".self::$appid."&"
	           ."siteid=0&"
	           ."version=515&"
	           ."IncludeSelector=Details,TextDescription&"
	           ."ItemID=".$ItemID;

	    $resp = simplexml_load_file($apicall);
	    
	    if($resp){
	        if ($resp->Ack == "Success") {         
	       
	            foreach($resp->Item as $item) {	                
	                if($item->PictureURL){
		                $ImageSetArr = array();
	                	foreach ($item->PictureURL as $k => $url) {	                		
	                		$ImageSetArr[] = $url;
	                	}                	
	                	$imageSet = implode('|',$ImageSetArr);	

	                	$description = (array)$item->Description;                
		            }
	            }                        
	        }
	    }

	    return array( 'imageSet'=>$imageSet,'description'=>$description );	    
	}


	  // Use itemId from selected most watched item as input for a GetSingleItem call
	private static function getSingleItemResults ($selectedItemID) {

	    $retnb  = '';

	    // Construct the GetSingleItem call 
	    $apicallb  = self::$s_endpoint;
	    $apicallb .= "callname=GetSingleItem";
	    $apicallb .= "&version=563";
	    $apicallb .= "&appid=".self::$appid;
	    $apicallb .= "&itemid=".$selectedItemID;
	    $apicallb .= "&responseencoding=".self::$responseEncoding;
	    $apicallb .= "&includeselector=Details,ShippingCosts";

	    // Load the call and capture the document returned by eBay API
	    $resp = simplexml_load_file($apicallb);
	    
	    // Check to see if the response was loaded, else print an error
	    if ($resp) {

	       // If there is a response check for a picture of the item to display
	      if ($resp->Item->PictureURL) {
	      $picURL = $resp->Item->PictureURL;
	      } else {
	      $picURL = "http://pics.ebaystatic.com/aw/pics/express/icons/iconPlaceholder_96x96.gif";
	      }

	      // Check for shipping cost information
	      if ($resp->Item->ShippingCostSummary->ShippingServiceCost) {
	      $shippingCost = "\$" . $resp->Item->ShippingCostSummary->ShippingServiceCost;
	      } else {
	      $shippingCost = "Not Specified";
	      }

	      // Build a table of item and user details for the selected most watched item
	      $retnb .= "<!-- start table in getSingleItemResults --> \n";
	      $retnb .= "<table width=\"100%\" cellpadding=\"5\"><tr> \n";
	      $retnb .= "<td self::$cellColor  width=\"50%\">\n";
	      $retnb .= "<div align=\"left\"> <!-- left align item details --> \n";
	      $retnb .= "Current price: <b>\$" . $resp->Item->ConvertedCurrentPrice . "</b><br> \n";
	      $retnb .= "Shipping cost: <b>" . $shippingCost . "</b><br>\n";
	      $retnb .= "Time left: <b>" . self::getPrettyTimeFromEbayTime($resp->Item->TimeLeft) . "</b><br> \n";
	      $retnb .= "</div></td> \n";
	      $retnb .= "<td $cellColor><div align=\"left\"> <!-- left align item details --> \n";
	      $retnb .= "Seller ID: <b>" . $resp->Item->Seller->UserID . "</b><br> \n";
	      $retnb .= "Feedback score: <b>" . $resp->Item->Seller->FeedbackScore . "</b><br> \n";
	      $retnb .= "Positive Feedback: <b>" . $resp->Item->Seller->PositiveFeedbackPercent . "</b><br>\n";
	      $retnb .= "</div></td></tr></table> \n<!-- finish table in getSingleItemResults --> \n"; 

	    } else {
	    // If there was no response, print an error
	    $retnb = "Dang! Must not have got the GetSingleItem response!";  
	    }  // if $resp

	    return $retnb;

	  } // End of getSingleItemResults function 


	  // Use itemId from selected most watched item as input for a getRelatedCategoryItems call
	private static function getRelatedCategoryItemsResults ($selectedItemID) {

	    // Construct the getRelatedCategoryItems call
	    $apicallc  = "self::$m_endpoint ";
	    $apicallc .= "OPERATION-NAME=getRelatedCategoryItems";
	    $apicallc .= "&SERVICE-VERSION=1.0.0";
	    $apicallc .= "&CONSUMER-ID=self::$appid ";
	    $apicallc .= "&RESPONSE-DATA-FORMAT=self::$responseEncoding ";
	    $apicallc .= "&maxResults=3";
	    $apicallc .= "&itemId=$selectedItemID";

	    // Load the call and capture the document returned by eBay API
	    $resp = simplexml_load_file($apicallc);
	    
	    // Check to see if the response was loaded, else print an error
	    if ($resp) {

	      $retnc = '';
	    
	    // Verify whether call was successful
	    if ($resp->ack == "Success") {

	        // If there were no errors, build a table for the 3 related category items
	        $retnc .= "<!-- start table in getRelatedCategoryItemsResults --> \n";
	        $retnc .= "<table width=\"100%\" cellpadding=\"5\" border=\"0\" bgcolor=\"#FFFFA6\"><tr> \n";
	        $retnc .= "<td colspan=\"3\"><b>eBay shoppers that liked items in the selected ";
	        $retnc .= "item's category also liked items like the following from related categories:</b>";
	        $retnc .= "</td></tr><tr> \n";
	        
	        // If the response was loaded, parse it and build links  
	        foreach($resp->itemRecommendations->item as $item) 
	        {
	        // For each item node, build a link and append it to $retnc
	        $retnc .= "<td valign=\"bottom\"> \n";
	        $retnc .= "<div align=\"center\"> <!-- center align item details --> \n";
	        $retnc .= "<img src=\"$item->imageURL\"> \n";
	        $retnc .= "<p><a href=\"" . $item->viewItemURL . "\">" . $item->title . "</a></p> \n";
	        $retnc .= "</div></td> \n";
	        } // foreach
	        $retnc .= "</tr></table> \n<!-- finish table in getRelatedCategoryItemsResults --> \n";

	    } else {
	      // If there were errors, print an error
	      $retnc  = "The response contains errors<br>";
	      $retnc .= "Call used was: $apicallc";
	    }  // if errors

	    } else {
	      // If there was no response, print an error
	      $retnc = "Dang! Must not have got the getRelatedCategoryItems response! <br> $apicallc";
	    }  // if $resp
	  
	    return $retnc;
	  
	  }  // End of getRelatedCategoryItemsResults function


	  // Make returned eBay times pretty
	private static function getPrettyTimeFromEbayTime($eBayTimeString){
	    // Input is of form 'PT12M25S'
	    $matchAry = array(); // null out array which will be filled
	    $pattern = "#P([0-9]{0,3}D)?T([0-9]?[0-9]H)?([0-9]?[0-9]M)?([0-9]?[0-9]S)#msiU";
	    preg_match($pattern, $eBayTimeString, $matchAry);
	    
	    $days = 0;
	    if(isset($matchAry[1])) $days  = (int) $matchAry[1];
	    $hours = 0;
	    if(isset($matchAry[2])) $hours = (int) $matchAry[2];
	    $min = 0;
	    if(isset($matchAry[3])) $min   = (int) $matchAry[3];  // $matchAry[3] is of form 55M - cast to int 
	    $sec = 0;
	    if(isset($matchAry[4])) $sec   = (int) $matchAry[4];
	    
	    $retnStr = '';
	    if ($days)  { $retnStr .= " $days day"   . self::pluralS($days);  }
	    if ($hours) { $retnStr .= " $hours hour" . self::pluralS($hours); }
	    if ($min)   { $retnStr .= " $min minute" . self::pluralS($min);   }
	    if ($sec)   { $retnStr .= " $sec second" . self::pluralS($sec);   }
	    
	    return $retnStr;
	  } // function

	private static function pluralS($intIn) {
	    // if $intIn > 1 return an 's', else return null string
	    if ($intIn > 1) {
	      return 's';
	    } else {
	      return '';
	    }
	} // function

	  

}
