<?php

class Etsy {

	public static function searchEx($category, $query, $sort, $page = 1) {
		
		// include(app_path().'/Libraries/simple_dom_html.php');

		$baseUrl = 'https://www.etsy.com/search'; //http://supermug.co.uk/collections/';
				
		$query = urlencode (utf8_encode($query));		
		
		if($category!='all'){
			$category = '/'. $category;
		}else{
			$category = '';
		}
	    
	    $url = $baseUrl . $category .'?q='. $query . '&order=' . $sort . '&page=' . $page;
	    
	    $response = ScraperNoSession::filterTextNoCache($url, ".block-grid-item");

	    if ($response == null)
			return null;

		$data = array();
		$ctr = 0; $rate = 1; $res = '';
		foreach ($response as $key => $row) {

			$sUrl 	= ScraperNoSession::getTagAttribute($row["raw"], 'a', 'href');
			$imgUrl = ScraperNoSession::getTagAttribute($row["raw"], 'img', 'src');
			$caption = trim(ucwords(ScraperNoSession::filterTextNoCache($row["raw"], ".card-meta-row", false)[0]["text"]));
			try {
				$currency = ScraperNoSession::filterTextNoCache($row["raw"], ".currency-symbol", false)[0]['text'];
			} catch (\Exception $e) {
				$currency = '';
			}

			$currency = str_replace('$', 'D', $currency); //replace AU$ with AUD, NZ$ with NZD, etc
			try {
				$price = ScraperNoSession::filterTextNoCache($row["raw"], ".currency", false)[0]['text'];
			} catch (\Exception $e) {
				$price = '';
			}
			$price = str_replace(',', '.', $price); 
			// $price = ScraperNoSession::filterTextNoCache($row["raw"], ".card-price", false)[0]['raw'];

			$plus = '';
			if($ctr==0 && $currency!='USD'){
				$res = self::getRate($currency);
				if($res!='invalid'){
					$rate = $res;
				}else{
					$rate = 1;
				}
			}			

			if($res!='invalid'){				
				$currency = 'USD';
				$price = $price / $rate;
				$plus = '+';				
			}
			
			$data[] = array(

					'thumbUrl' => trim($imgUrl),
					
					'sUrl' => $sUrl,

					'caption' => $caption, //tsy::trim_text($caption,85),

					'price' => (is_numeric($price)) ? $currency.' '.number_format($price,2).$plus : '',

					'targetUrl' => $url

					);

		$ctr++;
		}

		return $data;
	}

	private static function trim_text($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }
      
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }
      
        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);
      
        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }
      
        return $trimmed_text;
    }
	 
	private static function getRate($currency)
	{
		try {
		  	$url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USD'.$currency.'%22)&env=store://datatables.org/alltableswithkeys&format=json';
			// {"query":{"count":1,"created":"2017-01-18T08:11:49Z","lang":"en-US","results":{"rate":{"id":"USDEUR","Name":"USD/EUR","Rate":"0.9335","Date":"1/18/2017","Time":"1:00am","Ask":"0.9340","Bid":"0.9335"}}}}
			$res = ScraperNoSession::get($url, 'json', true);			
			if(isset($res['query']['count']) && $res['query']['count']=1 && isset($res['query']['results']['rate']['Rate'])){
				return $res['query']['results']['rate']['Rate'];
			}else{
				return 'invalid';
			}
		}
		//catch exception
		catch(Exception $e) {
		  	// echo 'Message: ' .$e->getMessage();
		  	return 'invalid';
		}				
	}

}
