<?php

class FacebookVideo {

	protected $fb;
	protected $fbs;
	protected $fb_login_config;

	public static function getFBConfig(){
		$data = array(
			  // live
			  'appId'  => '1311930458858344',
			  'secret' => '9c6abc34a9958f321f7c3f7da35f5956'
			  // 'appId'  => '1681919272049279',
			  // 'secret' => '1fc79b0445b9fd04d42de9f04c8214a4'
			  // 'appId'  => '798632360231414',
			  // 'secret' => '7c0b0b8abf3d4bd06c164ba5c1fa3390'
			  // dev Yhs&hy8887Trw72tg&&ujsh6
			  // 'appId'  => '489870057866439',
			  // 'secret' => '9255124ebb1ecbd843c787440dfa8535'
		);
		return $data;
	}

	public static function searchEx( $query ) {
		$fb_login_config = array('canvas' => 1,'fbconnect' => 1, 'scope' =>'');		
		$fb_config = self::getFBConfig();

		$fb = new Facebook($fb_config);
		$fbs = new fbsearch();

		$user = $fb->getUser();
		$viewData = array();
		$formData = Input::all();
		$str = '';
		$previous = $next = '';
		$data = array();

		if($user){		
					
			$fbs->init($fb->getAccessToken());
			
			
			$output = $fbs->search_facebook2($query);
			if(isset($output['paging'])){
				$paging = $output['paging'];
				
				if(isset($paging['next'])) $next = $paging['next'];
				if(isset($paging['previous'])) $previous = $paging['previous'];
			}
			
			$output = $output['data'];
			usort($output, 'sortByOrder');

			if(!empty($output) && count($output)>0){
				foreach ($output as $key => $row) {

					$data[] = array(
						'id' => $row['id'],
						'url' => "http://anonym.to/?https://www.facebook.com/".$row['id'],
						'thumbUrl' => $row['picture']['data']['url'],
						'caption' => str_replace('"','\"',$row['name']),
						'fullCaption' => $row['name'],						
						'likes' => isset( $row['likes']) ? number_format($row['likes'])  : '',
						'category' => isset( $row['category']) ? $row['category'] : ''						
					);					

				}
			}
		}
		
		// if(Session::get('email')=='ferdinand.borromeo@gmail.com'){
			echo json_encode(array('data'=>$data, 'next'=>$next, 'previous'=>$previous ));
		// }else{
			// print json_encode( $data );
		// }

    	exit;
	}

	public static function getPage(){
		$previous = $next = '';
		$data = array();

		$link = Input::get('link');
		$res = Scraper::get($link,'json',false);
		$output = $res['data'];
		if(isset($res['paging'])){
			$paging = $res['paging'];					
			if(isset($paging['next'])) $next = $paging['next'];
			if(isset($paging['previous'])) $previous = $paging['previous'];
		}

		usort($output, 'sortByOrder');

		if(!empty($output) && count($output)>0){
			foreach ($output as $key => $row) {

				$data[] = array(
					'id' => $row['id'],
					'url' => "http://anonym.to/?https://www.facebook.com/".$row['id'],
					'thumbUrl' => $row['picture']['data']['url'],
					'caption' => str_replace('"','\"',$row['name']),
					'fullCaption' => $row['name'],						
					'likes' => isset( $row['likes']) ? number_format($row['likes'])  : '',
					'category' => isset( $row['category']) ? $row['category'] : ''						
				);					

			}
		}

		echo json_encode(array('data'=>$data, 'next'=>$next, 'previous'=>$previous ));
		exit();
	}	  

}
