<?php
class fbsearch
{
	public $access_token;
	
	public function __construct(){
		//CI loader	
	}
	
	public function init($access_token)
	{
		$this->access_token = $access_token;
	}
	
	public function search_facebook($q)
	{
		$url = 'https://graph.facebook.com/v2.3/search?q='.urlencode($q).'&type=page&fields='.urlencode('likes,name,picture,category').'&limit=5000&access_token='.$this->access_token;
		$data = $this->curl_get($url);
		return json_decode($data, true);
	}

	//ferd custom
	public function search_facebook2($q)
	{
		$url = 'https://graph.facebook.com/v2.3/search?q='.urlencode($q).'&type=page&fields='.urlencode('likes.order(reverse_chronological),name,picture,category').'&limit=1000&access_token='.$this->access_token;		
		$data = $this->curl_get($url);
		return json_decode($data, true);
	}
	
	public function get_top_posts($page_id, $type='')
	{
		// $fql='limit=100&fields=from,message,link,source,name,picture,shares,comments.limit(1).summary(true),likes.limit(1).summary(true)';
		// $url = 'https://graph.facebook.com/v2.3/'.$page_id.'/'.$fql.'&access_token='.$this->access_token;
		$fql='limit=100&fields=from,message,link,source,description,picture,shares,comments.limit(1).summary(true),likes.limit(1).summary(true)';
		$url = 'https://graph.facebook.com/v2.3/'.$page_id.'/'.($type==128?'videos':'promotable_posts').'?'.$fql.'&access_token='.$this->access_token;

		$data = $this->curl_get($url);
		$data_array=json_decode($data, true);
		return $data_array;
	}
	
	public function curl_get($url)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);	
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$data = curl_exec($ch);
		return $data;
	}
	
}

function sortByOrder($a, $b) {
	if(isset($a['likes'])) {
		if(isset($b['likes'])){
			//dd($b['likes']);
			return count($b['likes']['data']) - count($a['likes']['data']);
		}
	}
    
}