<?php

use GuzzleHttp\Client as GuzzleClient;
use Goutte\Client as Client;
use Illuminate\Support\Str;

use Symfony\Component\DomCrawler\Crawler;

class ScraperNoSession {

    public static $client;

    private static $url;
    private static $html;
    private static $crawler;
    private static $filters;
    private static $content = array();
	private static $proxies = array();
	
	//protected static $super_proxy='';
	//protected static $super_proxy_session='';
	
	public static function getSuperProxy(){
		
		set_time_limit(0);
		
		//get super proxy	
		//$username = 'lum-customer-weblify-zone-gen-country-us'; // US ONLY
		$username = 'lum-customer-weblify-zone-gen';
		$password = 'bd5fd2dd5e6c';
		$session = mt_rand();
		$curl = curl_init("http://client.luminati.io/api/get_super_proxy?raw=1&user=$username&key=$password");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0); 
		curl_setopt($curl, CURLOPT_TIMEOUT, 400); //timeout in seconds
		$super_proxy = curl_exec($curl);
		$super_proxy_session="$username-session-$session:$password";	
		//test the super proxy here
		$info = curl_getinfo($curl);
		$code = $info['http_code'];
		//close
		curl_close($curl);
		if($code==403 || $code==429 || $code==502 || $code==503){
			//BAD Super Proxy LOG & Get Another One
			$error_array=
			array(
				'url' => 'Bad Super Proxy',
				'from_url'=> Request::url(),
				'superproxy_url' => $super_proxy,
				'error' => 'error code: '.$code
			); 			
			// DB::table('logs_scraper')->insert($error_array);
			// mail('elrickymataka@gmail.com','Failed Getting Good Super Proxy',print_r($error_array,true));
			// mail('ferdinand.borromeo@gmail.com','Failed Getting Good Super Proxy',print_r($error_array,true));
			// mail('vreyes.science@gmail.com','Failed Getting Good Super Proxy',print_r($error_array,true));
				
			 return self::getSuperProxy();
			
		}else{
			return array('super_proxy'=>$super_proxy, 'super_proxy_session'=>$super_proxy_session);
		}			
	}
	
    public static function useSuperProxy($url)
    {
	
		$res = self::getSuperProxy();
		$super_proxy = $res['super_proxy'];
		$super_proxy_session = $res['super_proxy_session'];
		
		try {
			
			$client = new Client(); //New Goutte
			//set curl options
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, 400);
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_RETURNTRANSFER, 1);
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_PROXY, "http://".$super_proxy.":22225");
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_PROXYUSERPWD, $super_proxy_session);
			$crawler = $client->request('GET', $url);
			
			$response_code=$client->getResponse()->getStatus();
			$headers_code=$client->getResponse()->getHeaders();
			
			$html=$crawler->html();	
		
		}
        catch(Exception $e){

		  $error_array=array(
			'url' => $url,
			// 'from_url'=> Request::url(),
			'superproxy_url' => $super_proxy,
			//'response_code' => $response_code,
			//'headers_code' => $headers_code,
			'error' => $e
			);
			
            DB::table('logs_scraper')->insert($error_array);

			// mail('elrickymataka@gmail.com','Curl Failed With Good Super Proxy',print_r($error_array,true));
			// mail('ferdinand.borromeo@gmail.com','curl died',print_r($error_array,true));
			// mail('vreyes.science@gmail.com','curl died',print_r($error_array,true));
			self::useSuperProxy($url);
			
        }
	

		// Continue to check
		if(isset($response_code) && $response_code!=200){
			
			//clear super proxy in session
			Session::forget('super_proxy');
			Session::forget('super_proxy_session');		
			self::getSuperProxy();
			self::useSuperProxy($url);
			
		}else if(isset($html) && strlen($html)<100){
			
			//clear super proxy in session
			Session::forget('super_proxy');
			Session::forget('super_proxy_session');		
			self::getSuperProxy();
			self::useSuperProxy($url);
			
		}else if(isset($html) && $html!=''){

			return $html;

		}else{
			return false;
		}
	}
	

    public static function get($url, $format = "html", $cache = true)
    {
    	$data = null;

		if ($format == "raw")
		{
			//Dont Use Proxis
			$client		= new Client;
			$crawler 	= $client->request('GET', $url);
			$data 		= $crawler->html();
		}			
		elseif ($format == "json")
		{	
			//If its JSON we call the Guzzle Client Directly			
			$data 		= array();
			$client 	= new GuzzleClient();
			try{
				$response 	= $client->get($url);
				if ( $response->getStatusCode() == 200 ) {
					$data = json_encode($response);
				}
			}catch (Exception $ex) {
				return $data;
			}					
		}
		else 
		{ 
			//HTML USE SUPER PROXY
			$data=self::useSuperProxy($url);
			
			//$client		= new Client;
			//$crawler 	= $client->request('GET', $url);
			//$data 		= $crawler->html();			
			
		}		

    	if($cache==true)
		{

			$cacheKey 		= Str::slug($url);
			$cacheMinutes 	= 60;
		
			if (!Cache::has($cacheKey))
			{
				Cache::put($cacheKey, $data, $cacheMinutes);
			}
			return Cache::get($cacheKey);
		}
		else
		{	
			//just return data
			return $data;
		}
    }
	
    /**
     * HTML Parsing & Get in one NO CACHE
     * @param  [string] $url   		[URL]
     * @param  [string] $xpath 		[XPath]
     * @return [string] $content	[HTML]
	 
	   // get means make the request
       // if not just crawl the contents
	 */
    public static function filterTextNoCache($html, $xpath, $get = true)
    {
    	//call client
		if($get){
			$crawler = new Crawler(self::get($html,'raw',false)); //turn off cache
		}else{
			$crawler = new Crawler($html);	
		}
		$content = $crawler->filter($xpath)->each(function(Crawler $node, $i) {
	    	return 	[
				"text" 	=> $node->text(),
				"raw"	=> $node->html()
			];
        });
        return $content;
    }



    /**
     * HTML Parsing & Get in one
     * @param  [string] $url   		[URL]
     * @param  [string] $xpath 		[XPath]
     * @return [string] $content	[HTML]
	 
	   // get means make the request
       // if not just crawl the contents
	   
	 */
    public static function filterText($html, $xpath, $get = true)
    {
    	//call client
		if($get){
			$crawler = new Crawler(self::get($html,'html',true)); // turn on cache
		}else{
			$crawler = new Crawler($html);	
		}
		$content = $crawler->filter($xpath)->each(function(Crawler $node, $i) {
	    	return 	[
				"text" 	=> $node->text(),
				"raw"	=> $node->html()
			];
        });
        return $content;
    }

    public static function getContent()
    {
    	return self::$content;
    }

    public static function getAnchorHref($html)
    {
    	return self::getTagAttribute($html, "a", "href");
    }

    public static function getImageSource($html)
    {

    	return self::getTagAttribute($html, "img", "src");
    }

    public static function getTagAttribute($html, $tag, $attribute)
    {

		$dom = new DOMDocument;

		@$dom->loadHTML($html, LIBXML_NOWARNING | LIBXML_ERR_NONE);
		
		$node = $dom->getElementsByTagName($tag);

        unset($dom);
        return $node->item(0)->getAttribute($attribute);
    }

    public static function getTagAttributeEx($html, $tag, $attribute, $index = 0)
    {

		$dom = new DOMDocument;

		@$dom->loadHTML($html, LIBXML_NOWARNING | LIBXML_ERR_NONE);
		
		$node = $dom->getElementsByTagName($tag);

        unset($dom);

        if( $node->item($index) === null ){
			return '';			
		}else{
			return $node->item($index)->getAttribute($attribute);
		}
    }

    public static function getTagText($html, $tag)
    {
        $dom = new DOMDocument;

        @$dom->loadHTML($html, LIBXML_NOWARNING | LIBXML_ERR_NONE);
        
        $node = $dom->getElementsByTagName($tag);

        unset($dom);
		return $node->item(0)->nodeValue;
    }
	

}