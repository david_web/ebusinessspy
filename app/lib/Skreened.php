<?php

class Skreened {

	public static function search($query, $page = 1)
	{
		$response = Scraper::filterTextNoCache("http://skreened.com/search/{$query}?page=$page", ".gallery-item");

		$data = self::parseItems($response);

		return $data;
	}
	
	private static function parseItems($response)
	{
		$data = array();
		
		foreach ($response as $key => $row) {

			$link 	= Scraper::getAnchorHref($row["raw"]);
			$thumb 	= Scraper::getTagAttribute($row["raw"], "img", "data-src");
			$title	= Scraper::getTagAttribute($row["raw"], "img", "alt");
			$rPrice	= Scraper::filterTextNoCache($row["raw"], ".caption-price", false);
						
			$price = '';
			if(isset($rPrice[0]["text"])){
				$price = trim(ucwords($rPrice[0]["text"]));
			}

			$data[] = [
				"link"	=> $link,
				"thumb"	=> $thumb,
				"title"	=> ucwords($title),
				"price"	=> $price,
			];
		}

		return $data;
	}
}