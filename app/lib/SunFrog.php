<?php

class SunFrog {	
	
	public static function searchEx($query, $page = 1)
	{
		if($page==1){
			$response = Scraper::filterTextNoCache("https://www.sunfrog.com/search/?cId=0&cName=&search={$query}",".frameitWrapper");
		}else{			
			$offset = ($page * 40) + 1;
			$response = Scraper::filterTextNoCache("https://www.sunfrog.com/search/paged2.cfm?schTrmFilter=popular&search={$query}&cID=0&offset={$offset}",".frameitWrapper");
		}
		
		if ($response == null)
			return null;

		$data = self::parseItemsEx($response);

		return $data;
	}
	
	private static function parseItemsEx($response)
	{		
		foreach ($response as $key => $row) {

			$rowData = $row["raw"];

			if ($rowData != null) {
				
				$link 	= Scraper::getTagAttribute($rowData, 'a', 'href');
				$frontThumb = Scraper::filterTextNoCache($rowData, ".frontThumb", false)[0]["raw"];
				$attrib = array("data-original","data-src","src");
				$thumb = '';
				for($c=0; $c<count($attrib); $c++){
					$thumb = Scraper::getTagAttribute($frontThumb, "img", $attrib[$c]);	
					if($thumb!=''){
						break;
					}
				}
				$title	= Scraper::getTagAttribute($frontThumb, "img", "title");				
				$price	= '';
				if(isset(Scraper::filterTextNoCache($rowData, ".price", false)[0]["text"])){
					$price = trim(Scraper::filterTextNoCache($rowData, ".price", false)[0]["text"]);
				}

				$data[] = [
					"link"	=> $link,
					"thumb"	=> $thumb,
					"title"	=> ucwords($title),
					"price"	=> $price,
				];
			}
		
		}

		return $data;
	}
}