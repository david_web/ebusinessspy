<?php

class SunFrogBestSellers {

	public static function searchEx($category)
	{			
		if($category!='all'){
			$response = Scraper::filterTextNoCache("https://www.sunfrog.com/{$category}", ".frameitWrapper");
		}else{			
			$response = ScraperNoSession::filterTextNoCache("https://www.sunfrog.com/Best-Sellers", ".frameitWrapper");
		}

		

		if ($response == null)
			return null;

		$data = self::parseItemsEx($response);

		return $data;
	}
	
	private static function parseItemsEx($response)
	{	

		foreach ($response as $key => $row) {

			$rowData = $row["raw"];

			if ($rowData != null) {
				$link 	= Scraper::getTagAttribute($rowData, 'a', 'href');
				$frontThumb = Scraper::filterTextNoCache($rowData, ".frontThumb", false)[0]["raw"];
				$thumb = Scraper::getTagAttribute($frontThumb, "img", "data-original");
				if($thumb=='') $thumb = Scraper::getTagAttribute($frontThumb, "img", "src");
				if (strpos($thumb,'https') === false || strpos($thumb,'sunfrog') === false ) {
					$thumb = Scraper::getTagAttribute($frontThumb, "img", "data-src");
				}
				$title	= Scraper::getTagAttribute($frontThumb, "img", "title");
				$price = '';
				if(isset(Scraper::filterTextNoCache($rowData, ".price", false)[0]["text"])){
					$price	= trim(Scraper::filterTextNoCache($rowData, ".price", false)[0]["text"]);
				}

				$data[] = [
					"link"	=> 'https://www.sunfrog.com/'.$link,
					"thumb"	=> $thumb,
					"title"	=> ucwords($title),
					"price"	=> $price,
				];
			}
		}

		return $data;
	}
	  

}
