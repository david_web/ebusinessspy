<?php

class TeeShirtTrending {

	public static function searchEx($category)
	{	
		if($category!='all'){
			$response = Scraper::filterTextNoCache("http://teespring.com/en-GB/shop/{$category}-tshirts", ".product_card");
			//temporary patch for categories returning empty list
			$tempList = array('dog','horse','NFL');
			if($response==null && in_array($category, $tempList)){
				//append 's'  and try again 
				if($category=='NFL'){
					$category = 'football';
				}else{
					$category = $category.'s';
				}
				$response = Scraper::filterTextNoCache("http://teespring.com/en-GB/shop/{$category}-tshirts", ".product_card");
			}
		}else{			
			$response = Scraper::filterTextNoCache("http://teespring.com/shop/trending-tshirts?utm_source=ts_homepage", ".product_card");
		}

		if ($response == null)
			return null;

		$data = self::parseItemsEx($response);

		return $data;
	}
	
	private static function parseItemsEx($response)
	{	
		$data = array();
		foreach ($response as $key => $row) {
			
			$rowData = $row["raw"];

			if ($rowData != null) {	

				$thumbLink 	= Scraper::filterTextNoCache($rowData, ".js-card", false)[0]["raw"];
					$thumb 	= Scraper::getTagAttributeEx($thumbLink, 'div', 'style', 0);
					$thumb = str_replace('background-image: url(','',$thumb);
					$thumb = str_replace(')','',$thumb);	
				$title	= trim(Scraper::filterTextNoCache($rowData, ".product_card__title", false)[0]["text"]);	
				$price	= trim(Scraper::filterTextNoCache($rowData, ".product_card__price", false)[0]["text"]);								
				$link 	= Scraper::getTagAttributeEx($rowData, 'a', 'href', 1);		

				if($thumb==''){
					$imgContainer 	= Scraper::filterTextNoCache($rowData, ".product_card__image_container", false)[0]["raw"];
					$thumb = Scraper::getTagAttributeEx($imgContainer, 'img', 'src', 0);
				}
								
				$data[] = [
					"link"	=> $link,//https://teespring.com'.$link,
					"thumb"	=> 'http:'.$thumb,
					"title"	=> ucwords($title),
					"price"	=> $price
				];
			}
		}

		return $data;
	}

	private static function addItemLinks($response, $data)
	{			
		$dom = new DOMDocument;
		@$dom->loadHTML($response, LIBXML_NOWARNING | LIBXML_ERR_NONE);		
		$node = $dom->getElementsByTagName('a');
		unset($dom);

		$ctr = 0;
		for($i=0; $i<$node->length; $i++){
			$link = $node->item($i)->getAttribute('href');

			if(strpos($link,"src")){
				$data[$ctr]['link'] = 'https://teespring.com'.$link;
				$ctr++;
			}
		}
		return $data;
	}
	  

}
