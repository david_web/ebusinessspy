<?php

class TeeSpring {
	
	public static function searchEx($query, $page = 1)
	{		
		// echo "https://teespring.com/en-GB#q={$query}&p={$page}"; exit();
		$response = Scraper::filterTextNoCache("https://teespring.com/en-GB#q={$query}&p={$page}", ".product_card");

		if ($response == null)
			return null;

		$data = self::parseItemsEx($response);

		return $data;
	}
	
	private static function parseItemsEx($response)
	{	

		foreach ($response as $key => $row) {

			$rowData = $row["raw"];

			if ($rowData != null) {
				$link 	= Scraper::getTagAttribute($rowData, 'a', 'href');

				$thumb 	= Scraper::filterTextNoCache($rowData, ".product_card__image_container", false)[0]["raw"];
					$thumbArray = explode("src=\"", $thumb);
					$thumbArray = explode("\"", $thumbArray[1]);
					$thumb 	= $thumbArray[0];
				$title	= trim(Scraper::filterTextNoCache($rowData, ".product_card__title", false)[0]["text"]);
				$price	= trim(Scraper::filterTextNoCache($rowData, ".product_card__price", false)[0]["text"]);
					$price = str_replace('Â£', '', $price);
					$price = str_replace('â¬', '', $price);
					$price = str_replace('$', '', $price);
				$sold = Scraper::filterTextNoCache($rowData, ".product_card_sold", false)!=[] ? trim(Scraper::filterTextNoCache($rowData, ".product_card_sold", false)[0]["text"]):'';
				$daysLeft = trim(Scraper::filterTextNoCache($rowData, ".product_card__timer", false)[0]["text"]);								
					
				$data[] = [
					"link"	=> 'https://teespring.com'.$link,
					"thumb"	=> $thumb,
					"title"	=> ucwords($title),
					"price"	=> $price,
					"sold"	=> $sold,
					"daysLeft" => $daysLeft
				];
			}
		}

		return $data;
	}
}