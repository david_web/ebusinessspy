<?php

class Vimeo {

    public static function searchEx($query, $page){

        $consKey = '44ca6daffcd83a9e230142b494c97938b4e20252';
        $secret = '2f2282117c0b7d5f0c694d0ff6debf6c4cc52c83';
        $vimeo = new phpVimeo($consKey,$secret);
        
        $youtubequery = urlencode(strtolower($query));        
        $videos = $vimeo->call('vimeo.videos.search', array('query' => $youtubequery,'page' => $page,'per_page' => 12,'sort'=>'plays','direction'=>'desc'));    

        $data = [];
                
        if($videos->videos->total>0){
            
            foreach($videos->videos->video as $index=>$val){
                $videoInfo = self::curl_get($val->id);
                if(isset($videoInfo[0]['stats_number_of_plays'])){
                    $view = $videoInfo[0]['stats_number_of_plays'];
                    $kview = $videoInfo[0]['stats_number_of_plays'];
                } else {
                    $view = $index;
                    $kview = 0;
                }
                
                if(isset($videoInfo[0]['stats_number_of_likes'])){
                    $likes = $videoInfo[0]['stats_number_of_likes'];
                } else {
                    $likes = 0;
                }
                
                $description = '';
                if(isset($videoInfo[0]['description'])){
                    $description = $videoInfo[0]['description'];
                    $description = strip_tags( str_replace("'","",$description) );
                }

                $data[] = [
                    "id" => $val->id,
                    "views" => number_format($kview),
                    "duration" => isset($videoInfo[0]['duration']) ? YouTube::yt_duration($videoInfo[0]['duration']) : '',
                    "description" => $description,
                    "likes" => number_format($likes),
                    "title" => str_replace("'","",$val->title),
                    "url" => isset($videoInfo[0]['url']) ? $videoInfo[0]['url'] : '',
                    "thumbnail_medium" => isset($videoInfo[0]['thumbnail_medium']) ? $videoInfo[0]['thumbnail_medium'] : '',
                    "thumbnail_large" => $videoInfo[0]['thumbnail_large']
                ];            
            }
        }
        
        return $data;
    }
    
    public static function curl_get($id) {
        $url = 'vimeo.com/api/v2/video/'.$id.'.json';
        $res = Scraper::get($url,'json',false);
        return $res;
    }
    
}