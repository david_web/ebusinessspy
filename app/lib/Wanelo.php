<?php

class Wanelo {

	public static function searchEx($category, $sort, $query, $page = 1) {
				
		$baseUrl = 'https://wanelo.com';

		$query = urlencode (utf8_encode($query));

	    $url = $baseUrl .'/search?query=' . $query;
	    $url .= '&page=' . $page;	    
	    if($category!='all'){
	    	$url .= '&style=' . $category; 
	    }    	 

	    $response = ScraperNoSession::filterTextNoCache($url, ".js-paginated-product");

	    if ($response == null)
			return null;

		$data = array();
		foreach ($response as $key => $row) {

			$sUrl 	= $baseUrl.ScraperNoSession::getTagAttribute($row["raw"], 'a', 'href');
			$imgUrl 	= ScraperNoSession::getTagAttribute($row["raw"], 'img', 'src');
			$caption = trim(ucwords(ScraperNoSession::filterTextNoCache($row["raw"], ".product-info", false)[0]["text"]));
			$price = trim(ucwords(ScraperNoSession::filterTextNoCache($row["raw"], ".price", false)[0]["text"]));
			$savesCount = trim(ucwords(ScraperNoSession::filterTextNoCache($row["raw"], ".js-saves-count", false)[0]["text"]));
			$savesCount = (int)str_replace(',', '', $savesCount);
			
			$data[] = array(

					'thumbUrl' => trim($imgUrl),
					
					'sUrl' => $sUrl,

					'caption' => $caption, //Wanelo::trim_text($caption,55),

					'price' => trim($price),

					'savesCount' => number_format($savesCount),

					'saveCount' => $savesCount

					);
		}		

		$saveCntArr = array();
		foreach ($data as $key => $row)
		{
		    $saveCntArr[$key] = $row['saveCount'];
		}

		if($sort=='save_asc'){
			array_multisort($saveCntArr, SORT_ASC, $data);
		}else{
			array_multisort($saveCntArr, SORT_DESC, $data);
		}				

		return $data;
	}


	private static function trim_text($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }
      
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }
      
        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);
      
        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }
      
        return $trimmed_text;
    }
	  

}
