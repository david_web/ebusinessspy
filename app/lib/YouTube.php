<?php

class YouTube {

	public static function searchEx($min_views, $common_vid, $query, $page) {
		
		
		$obj = new \Yt();		
		if($page!='start'){
			$output = $obj->route( trim($query), trim($common_vid), trim($page));
		}else{
			$output = $obj->route( trim($query), trim($common_vid));
		}

		$data = array(); $info = '';

		if(!empty($output['results']) && count($output['results'])>0){
			foreach ($output['results'] as $key => $row) {
				$data[] = [
					'thumbUrl' => $row['video_data']['snippet']['thumbnails']['high']['url'],
					'caption' => str_replace( "'","", self::trim_text( $row['video_data']['snippet']['title'], 50 )),
					'fullCaption' => str_replace( "'","",$row['video_data']['snippet']['title'] ),
					'videoUrl' => "https://www.youtube.com/embed/".$row['video_data']['id']."?autoplay=1",
					'description' => $row['video_data']['snippet']['description'],
					'duration' => self::yt_duration( $row['video_data']['contentDetails']['duration'] ),
					'views' => isset( $row['video_data']['statistics']['viewCount']) ? number_format( $row['video_data']['statistics']['viewCount'] ) : '',
					'likes' => isset( $row['video_data']['statistics']['likeCount']) ? number_format( $row['video_data']['statistics']['likeCount'] ) : ''
				];
			}
		}

		if(!empty($output['info'])){
			$info = $output['info'];
		}
		//dd($info);
		return ['data'=>$data, 'info'=>$info];

    	exit;
	}

	//temporary patch for youtube at "What's trending now" section
	public static function searchTrending($query, $token) {		
		
		$obj = new Yt();		
		$common_vid = 'no';
		if($token=='start'){
			$output = $obj->route( trim($query), trim($common_vid));
		}else{
			$output = $obj->route( trim($query), trim($common_vid), trim($token));
		}

		$data = array(); $info = '';

		if(!empty($output['results']) && count($output['results'])>0){
			foreach ($output['results'] as $key => $row) {

				$data[] = array(

					'thumbUrl' => $row['video_data']['snippet']['thumbnails']['high']['url'],
					'caption' => self::trim_text( $row['video_data']['snippet']['title'], 50 ),	
					'fullCaption' => str_replace( "'","",$row['video_data']['snippet']['title'] ),				
					'videoUrl' => "https://www.youtube.com/embed/".$row['video_data']['id']."?autoplay=1",
					'description' => $row['video_data']['snippet']['description'],					
					'duration' => self::yt_duration( $row['video_data']['contentDetails']['duration'] ),
					'views' => isset( $row['video_data']['statistics']['viewCount']) ? number_format( $row['video_data']['statistics']['viewCount'] ) : '',
					'likes' => isset( $row['video_data']['statistics']['likeCount']) ? number_format( $row['video_data']['statistics']['likeCount'] ) : ''					
				);			

			}
		}

		if(!empty($output['info'])){
			$info = $output['info'];
		}
		
		print json_encode( array('data'=>$data, 'info'=>$info) );

    	exit;
	}

	public static function searchMostPopular() {
		
		
		$obj = new Yt();		
		
		$output = $obj->getPopularVideos('US');		

		$data = []; $info = '';

		if(!empty($output) && count($output)>0){
			foreach ($output as $key => $row) {

				$data[] = array(

					'thumbUrl' => $row['snippet']['thumbnails']['high']['url'],
					'caption' => str_replace( "'","", self::trim_text( $row['snippet']['title'], 50 )),
					'fullCaption' => str_replace( "'","",$row['snippet']['title'] ),					
					'videoUrl' => "https://www.youtube.com/embed/".$row['id']."?autoplay=1",
					'description' => $row['snippet']['description'],					
					'duration' => self::yt_duration( $row['contentDetails']['duration'] ),
					'views' => isset( $row['statistics']['viewCount']) ? number_format( $row['statistics']['viewCount'] ) : '',
					'likes' => isset( $row['statistics']['likeCount']) ? number_format( $row['statistics']['likeCount'] ) : ''
					
				);			

			}
		}		
		
		return ['data'=>$data, 'info'=>$info];

    	exit;
	}

	public static function yt_duration($youtube_time) {
		preg_match_all('/(\d+)/',$youtube_time,$parts);
	
		// Put in zeros if we have less than 3 numbers.
		if (count($parts[0]) == 1) {
			array_unshift($parts[0], "0", "0");
		} elseif (count($parts[0]) == 2) {
			array_unshift($parts[0], "0");
		}
	
		$sec_init = $parts[0][2];
		$seconds = $sec_init%60;
		$seconds_overflow = floor($sec_init/60);
	
		$min_init = $parts[0][1] + $seconds_overflow;
		$minutes = ($min_init)%60;
		$minutes_overflow = floor(($min_init)/60);
	
		$hours = $parts[0][0] + $minutes_overflow;
	
		if($hours != 0)
			return $hours.':'.$minutes.':'.$seconds;
		else
			return $minutes.':'.$seconds;
	}


	private static function trim_text($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }
      
        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }
      
        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);
      
        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }
      
        return $trimmed_text;
    }
	  

}
