<?php
/**
 * Configuration settings used by all of the examples.
 *
 * Specify your eBay application keys in the appropriate places.
 *
 * Be careful not to commit this file into an SCM repository.
 * You risk exposing your eBay application keys to more people than intended.
 */
return [
    'sandbox' => [
        'credentials' => [
            'devId' => 'c079e915-bd9a-44bc-9421-1c71fb5d296b',
            'appId' => 'MherKul-shoplica-SBX-42466ad0e-a4ed73b8',
            'certId' => 'SBX-2466ad0e35a9-ed4d-43a0-aa76-6aff',
        ],
        'authToken' => 'YOUR_SANDBOX_USER_TOKEN_APPLICATION_KEY',
        'oauthUserToken' => 'YOUR_SANDBOX_OAUTH_USER_TOKEN'
    ],
    'production' => [
        'credentials' => [
            'devId' => 'c079e915-bd9a-44bc-9421-1c71fb5d296b',
            'appId' => 'MherKul-shoplica-PRD-e2466ad0e-0de7a58c',
            'certId' => 'PRD-2466ad0e40b5-5416-4aeb-86c0-a085',
        ],
        'authToken' => 'YOUR_PRODUCTION_USER_TOKEN_APPLICATION_KEY',
        'oauthUserToken' => 'YOUR_PRODUCTION_OAUTH_USER_TOKEN'
    ]
];
