<?php

use Illuminate\Database\Seeder;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('shops')->insert([
        //     'name' => 'eBay',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Amazon',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'AliExpress',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Wanelo',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Etsy',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Youtube',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Vimeo',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Facebook',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Daily Motion',
        // ]);

        // DB::table('shops')->insert([
        //     'name' => 'Media',
        // ]);

        DB::table('shops')->insert([
            'name' => 'Shopify',
        ]);
    }
}
