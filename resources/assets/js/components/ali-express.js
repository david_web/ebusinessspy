Vue.component('ali-express', {
    props: ['user'],

    data() {
        return {
            isLoadingSites : false,
            aliExpressProducts : [],
            searchResults : 'test',
            categorySearch : 'all',
            sortSearch : 'any',
            page: 1,
            seen: false,
            searchKeys: [],
            sorts: [
                {text: 'Order by ...', value: 'any'},
                {text: 'by Original Price: Low To High', value: 'orignalPriceUp'},
                {text: 'by Original Price:  High To Low', value: 'orignalPriceDown'}
            ],
            categories: [
                { text: 'Select category', value: 'all' },
                { text: 'All', value: 'all' },
                { text: 'Apparel Accessories', value: '3' },
                { text: 'Automobiles Motorcycles', value: '34' },
                { text: 'Baby Products', value: '1501' },
                { text: 'Beauty Health', value: '66' },
                { text: 'Computer Networking', value: '7' },
                { text: 'Construction Real Estate', value: '13' },
                { text: 'Consumer Electronics', value: '44' },
                { text: 'Customized Products', value: '100008578' },
                { text: 'Electrical Equipment Supplies', value: '5' },
                { text: 'Electronic Components Supplies', value: '502' },
                { text: 'Food', value: '2' },
                { text: 'Furniture', value: '1503' },
                { text: 'Hair Accessories', value: '200003655' },
                { text: 'Hardware', value: '42' },
                { text: 'Home Garden', value: '15' },
                { text: 'Home Appliances', value: '6' },
                { text: 'Industry Business', value: '200003590' },
                { text: 'Jewelry Watch', value: '36' },
                { text: 'Lights Lighting', value: '39' },
                { text: 'Luggage Bags', value: '1524' },
                { text: 'Office School Supplies', value: '21' },
                { text: 'Phones Telecommunications', value: '509' },
                { text: 'Security Protection', value: '30' },
                { text: 'Shoes', value: '322' },
                { text: 'Special Category', value: '200001075' },
                { text: 'Sports Entertainment', value: '18' },
                { text: 'Tools', value: '1420' },
                { text: 'Toys Hobbies', value: '26' },
                { text: 'Watches', value: '1511' },
            ],
            error: '',
            errorShow: false
        };
    },

    methods: {
        search: function () {
            this.isLoadingSites = true;
            // if (this.searchResults == '') {
            //     this.errorShow = true;
            //     this.error = 'Search parametr is required!';
            // } else {
                this.errorShow = false;
                this.$http.get('/ali-express-search/' + this.searchResults + '/' + this.categorySearch + '/' + this.sortSearch)
                    .then(response => {
                        this.aliExpressProducts = response.data.aliExpressProducts;
                        this.searchKeys = response.data.searchKeys;
                        this.isLoadingSites = false;
                        if (response.data.aliExpressProducts.length == 0) {
                            this.seen = false;
                        } else {
                            this.seen = true;
                        }
                    });
            // }
        },

        clearSearch: function () {
            this.searchResults = '';
        },

        loadAliExpressMore: function () {
            this.page += 1;
            this.$http.get('/ali-express-search/' + this.searchResults + '/' + this.categorySearch + '/' + this.sortSearch + '/' + this.page)
                .then(response => {
                    this.aliExpressProducts = this.aliExpressProducts.concat(response.data.aliExpressProducts);

                    this.isLoadingSites = false;
                    this.seen = true;
                });
        },

        searchByKeyWord: function(key_word) {
            this.searchResults = key_word;
            this.search();
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 3
            };
            Bus.$emit('add-search-key', data);
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 3
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    mounted() {
        var that = this;
        Bus.$on('search-by-key-word', function (keyWord) {
            that.searchResults = keyWord;
            that.search();
        });
        Bus.$emit('is-search', true);
        this.search();
    }
});