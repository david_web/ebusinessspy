Vue.component('amazon', {
    props: ['user'],

    data() {
        return {
            isLoadingSites : false,
            amazonProducts : [],
            searchResults : 'tshirt',
            seen: false,
            page: 1,
            reviewItem: false,
            reviewItemIframe: '',
            searchKeys: [],
        };
    },

    methods: {
        search: function () {
            this.isLoadingSites = true;

            this.$http.get('/amazon-search/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.amazonProducts = response.data.amazonProducts;
                    this.searchKeys = response.data.searchKeys;
                    this.isLoadingSites = false;
                    this.seen = true;
                });
        },

        clearSearch: function () {
            this.searchResults = '';
        },

        loadAmazonMore: function () {
            this.page += 1;
            this.$http.get('/amazon-search/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.amazonProducts = this.amazonProducts.concat(response.data.amazonProducts);

                    this.isLoadingSites = false;
                    this.seen = true;
                });
        },
        reviewModal: function (IFrameURL) {
            this.reviewItem = true;
            this.reviewItemIframe = IFrameURL;
            $('#modal-reviews').modal('show');
        },

        searchByKeyWord: function(key_word) {
            this.searchResults = key_word;
            this.search();
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 2
            };
            Bus.$emit('add-search-key', data);
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 2
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    mounted() {
        var that = this;
        Bus.$on('search-by-key-word', function (keyWord) {
            that.searchResults = keyWord;
            that.search();
        });
        Bus.$emit('is-search', true);
        this.search();
    }
});