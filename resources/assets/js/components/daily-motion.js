Vue.component('daily-motion-videos', {
    props: ['user'],

    data() {
        return {
            searchResults : 'popular',
            dailyProducts : [],
            caption: '',
            description: '',
            infoItem: false,
            selectedVideoUrl: '',
            selectedVideoCaption: '',
            videoItem: false,
            page: 1,
            seen: false
        };
    },

    methods: {
        search: function () {
            this.loading = true;
            this.$http.get('/daily-motion-video-search/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.dailyProducts = response.data;
                    this.loading = false;
                    this.seen = true;
                });
        },

        loadDailyMore: function () {
            this.page += 1;
            this.$http.get('/daily-motion-video-search/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.dailyProducts = this.dailyProducts.concat(response.data);
                    this.loading = false;
                    this.seen = true;
                });
        },

        clearSearch: function () {
            this.searchResults = ''
        },

        infoModal: function (capt, desc) {
            this.caption = capt;
            this.description = desc;
            this.infoItem = true;
            $('#modal-info').modal('show');
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 9
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 9
            };
            Bus.$emit('add-search-key', data);
        }
    },

    mounted() {
        Bus.$emit('is-search', true);
        this.search();
    }
});