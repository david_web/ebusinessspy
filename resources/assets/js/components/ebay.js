Vue.component('ebay', {
    props: ['user'],

    data() {
        return {
            loading : false,
            ebayProducts : [],
            searchResults : '',
            categorySearch : 'none',
            sortSearch : 'none',
            orderSearch : 'none',
            searchKeys: [],
            categories: [
                { text: 'Select category', value: 'none' },
                { text: 'Antiques', value: '20081' },
                { text: 'Art', value: '550' },
                { text: 'Baby', value: '2984' },
                { text: 'Books', value: '267' },
                { text: 'Business, Industrial', value: '12576' },
                { text: 'Cameras, Photo', value: '625' },
                { text: 'Clothing, Shoes, Accessories', value: '11450' },
                { text: 'Coins, Currency', value: '11116' },
                { text: 'Collectibles', value: '1' },
                { text: 'Computers, Tablets', value: '58058' },
                { text: 'Consumer Electronics', value: '293' },
                { text: 'Crafts', value: '14339' },
                { text: 'DVD, Movies', value: '11232' },
                { text: 'Entertainment Memorabilia', value: '45100' },
                { text: 'Health, Beauty', value: '26395' },
                { text: 'Home, Garden', value: '11700' },
                { text: 'Jewellery, Watches', value: '281' },
                { text: 'Mobile Phones, Accessories', value: '15032' },
                { text: 'Music', value: '11233' },
                { text: 'Musical Instruments', value: '173484' },
                { text: 'Pottery, Glass', value: '870' },
                { text: 'Real Estate', value: '10542' },
                { text: 'Sporting Goods', value: '382' },
                { text: 'Sporting Memorabilia', value: '64482' },
                { text: 'Stamps', value: '260' },
                { text: 'Toys, Hobbies', value: '220' },
                { text: 'Vehicles', value: '9800' },
                { text: 'Vehicle Parts, Accessories', value: '6028' },
                { text: 'Video Games', value: '1249' },
                { text: 'Deal Vouchers, Gift Cards', value: '172008' },
                { text: 'Everything Else', value: '99' },
            ],
            sorts: [
                { text: 'Select sort', value: 'none' },
                { text: 'Price', value: 'price' },
                { text: 'Past Sales', value: 'pastSales' },
                { text: 'Watch Count', value: 'watchCount' }
            ],
            orders: [
                { text: 'Select order', value: 'none' },
                { text: 'High to Low', value: 'desc' },
                { text: 'Low to High', value: 'asc' }
            ]
        };
    },

    methods: {
        search: function () {
            this.loading = true;
            if (this.searchResults != '' ||
                this.categorySearch != 'none' || 
                this.sortSearch != 'none' || 
                this.orderSearch != 'none')
            {
                var searchResults = this.searchResults == ''?'undefined':this.searchResults;
                this.$http.get('/ebay-search/'+searchResults+'/'+this.categorySearch+'/'+this.sortSearch+'/'+this.orderSearch)
                    .then(response => {
                        this.ebayProducts = response.data.mostPopularEbay;
                        this.searchKeys = response.data.searchKeys;
                        this.loading = false;
                    });
            } else {
                this.$http.get('/ebay-search')
                    .then(response => {
                        this.ebayProducts = response.data.mostPopularEbay;
                        this.searchKeys = response.data.searchKeys;
                        this.loading = false;
                    });
            }
        },

        clearSearch: function () {
            this.searchResults = '';
        },

        searchByKeyWord: function(key_word) {
            this.searchResults = key_word;
            this.search();
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 1
            };
            Bus.$emit('add-search-key', data);
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 1
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    mounted() {
        var that = this;
        Bus.$on('search-by-key-word', function (keyWord) {
            that.searchResults = keyWord;
            that.search();
        });
        Bus.$emit('is-search', true);
        this.search();
    }
});