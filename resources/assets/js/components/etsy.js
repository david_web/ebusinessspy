Vue.component('etsy', {
    props: ['user'],

    data() {
        return {
            products : [],
            searchResults : 'watch',
            categorySearch : 'all',
            sortSearch : 'date_desc',
            page: 1,
            seen: false,
            searchKeys: [],
            sorts: [
                {text: 'by Date: Low To High', value: 'date_asc'},
                {text: 'by Date: High To Low', value: 'date_desc'}
            ],
            categories: [
                { text: 'All Categories', value: 'all' },
                { text: 'Accessories', value: 'accessories' },
                { text: 'Art &amp; Collectibles', value: 'art-and-collectibles' },
                { text: 'Bags &amp; Purses', value: 'bags-and-purses' },
                { text: 'Bath &amp; Beauty', value: 'bath-and-beauty' },
                { text: 'Books, Movies &amp; Music', value: 'books-movies-and-music' },
                { text: 'Clothing', value: 'clothing' },
                { text: 'Craft Supplies &amp; Tools', value: 'craft-supplies-and-tools' },
                { text: 'Electronics &amp; Accessories', value: 'electronics-and-accessories' },
                { text: 'Home &amp; Living', value: 'home-and-living' },
                { text: 'Jewelry', value: 'jewelry' },
                { text: 'Paper &amp; Party Supplies', value: 'paper-and-party-supplies' },
                { text: 'Pet Supplies', value: 'pet-supplies' },
                { text: 'Shoes', value: 'shoes' },
                { text: 'Toys &amp; Games', value: 'toys-and-games' },
                { text: 'Weddings', value: 'weddings' },
            ],
            error: '',
            errorShow: false
        };
    },

    methods: {
        search: function () {
            // if (this.searchResults == '') {
            //     this.errorShow = true;
            //     this.error = 'Search parametr is required!';
            // } else {
                this.errorShow = false;
                this.$http.get('/etsy-search/' + this.categorySearch + '/' + this.searchResults + '/' + this.sortSearch)
                    .then(response => {
                        this.products = response.data.etsyProducts;
                        this.searchKeys = response.data.searchKeys;
                        this.seen = true;
                    });
            // }
        },

        clearSearch: function () {
            this.searchResults = '';
        },

        loadMore: function () {
            this.page += 1;
            this.$http.get('/etsy-search/' + this.categorySearch + '/' + this.searchResults + '/' + this.sortSearch + '/' + this.page)
                .then(response => {
                    this.products = this.products.concat(response.data.etsyProducts);

                    this.seen = true;
                });
        },

        searchByKeyWord: function(key_word) {
            this.searchResults = key_word;
            this.search();
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 5
            };
            Bus.$emit('add-search-key', data);
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 5
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    mounted() {
        var that = this;
        Bus.$on('search-by-key-word', function (keyWord) {
            that.searchResults = keyWord;
            that.search();
        });
        Bus.$emit('is-search', true);
        this.search();
    }
});