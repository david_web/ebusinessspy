Vue.component('facebook-ads', {
    props: ['user'],

    data() {
        return {
            keyWord: '',
            website: '',
            postType: 'recent',
            timePeriod: 'this-week',
            url: '',
        };
    },

    methods: {
        clearSearch: function () {
            this.keyWord = ''
        }
    },

    computed: {
            computedUrl: function () {
                var that = this;

                if(that.website.length > 0){
                  var website = that.website.replace('://','%3A%2F%2F');
                }

                var url = 'https://www.facebook.com/search/str/' + website + '%2F' + that.keyWord;

                switch(that.postType){
                    case 'recent':
                        url += '/stories-keyword/'+that.timePeriod+'/date/stories/intersect';
                    case 'all': url += '/stories-keyword/intersect'; break;
                    case 'live': url += '/stories-keyword/intersect/stories-live'; break;
                    case 'pages': url += '/stories-keyword/stories-publishers'; break;
                    case 'shared': url += '/stories-keyword/share/stories/intersect'; break;
                    case 'photos': url += '/stories-keyword/photo/stories/intersect'; break;
                    case 'videos': url += '/stories-keyword/video/stories/intersect'; break;
                }
                return url;
            }
    },

    mounted() {
        
    }
});