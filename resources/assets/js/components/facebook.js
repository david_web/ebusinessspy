Vue.component('facebook-videos', {
    props: ['user'],

    data() {
        return {
            searchResults : '',
            loading: false,
            fbProducts : [],
            topFbPosts: [],
            topItem: false,
            topName: ''
        };
    },

    methods: {
        search: function () {
            this.loading = true;
            this.$http.get('/fb-videos-search/' + this.searchResults)
                .then(response => {
                    this.fbProducts = response.data;
                    //this.searchKeys = response.data.searchKeys;
                    this.loading = false;
                });
        },

        clearSearch: function () {
            this.searchResults = ''
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 8
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 8
            };
            Bus.$emit('add-search-key', data);
        },

        getTopPosts: function (id, type, name) {
            this.loading = true;
            this.topName = name;
            var data = {
                page_id: id,
                post_type: type
            };
            this.$http.post('/fb-top-search', data)
                .then(response => {
                    this.topFbPosts = response.data;
                    this.loading = false;
                    this.topModal();
                });
        },

        topModal: function () {
            this.topItem = true;
            $('#modal-info').modal('show');
        }
    },

    mounted() {
        Bus.$emit('is-search', true);
    }
});