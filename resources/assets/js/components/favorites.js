Vue.component('favorites', {
    props: ['user'],

    data() {
        return {
            loading : false,
            searchKeys: [],
            searchItems: [],
            favoriteItems: [],
            shops: {},
            shopId: 1,
            page: 'media',
            active: {}
            // keyWords: 0

        };
    },

    methods: {
        getData: function () {
            this.$http.get('/favorite-items')
                .then(response => {
                    var shops = {};
                    var active = {};
                    response.data.shops.forEach(function(value, key) {
                        shops[value.id] = value;

                        value.favorite_items.forEach(function(favoriteValue) {
                            active[favoriteValue.id] = false;
                        });
                    });
                    this.shops = shops;
                    this.active = active;
                });
        },

        changeShop: function (id) {
            this.shopId = id;
        },

        changePage: function (page) {
            this.page = page;
        },

        deleteItem: function (id) {
            var that = this;
            this.$http.get('/favorites/delete-favorite-item/' + id)
                .then(response => {
                    if (response.data.result == 'success') {
                        this.getData();
                    }
                });
        },

        deleteKeyWord: function(id) {
            this.$http.get('/favorites/delete-search-key/' + id)
                .then(response => {
                    if (response.data.result == 'success') {
                        this.getData();
                    }
                });
        },

        mouseOver: function(id) {
            this.active[id] = true;
        },

        mouseUp: function(id) {
            this.active[id] = false;
        }
    },

    mounted() {
        this.getData();

    },

    computed: {
    }

});