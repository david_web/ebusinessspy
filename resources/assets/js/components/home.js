Vue.component('home', {
    props: ['user'],

    data() {
        return {
            contactItem: false,
            emailType: '',
            emailSubject: '',
            emailMessage: ''
        };
    },

    methods: {
    	contactUsModal: function () {
            this.contactItem = true;
            $('#modal-contact-us').modal('show');
    	},

    	submitMessage: function () {
    		var data = {
    			contact_type: this.emailType,
    			contact_subject: this.emailSubject,
    			contact_message: this.emailMessage
    		}

    		this.$http.post('/contact-us', data)
                .then(response => {
                	if (response.data.success == true) {
                		$('#modal-contact-us').modal('hide');
                	}
                });
    	}
    },

    mounted() {
        //
    }
});
