Vue.component('search-keys', {
    props: ['user'],

    data() {
        return {
            searchKeys: [],
            isSearch: false
        }
    },

    methods: {
        getSearchKeys: function () {
            this.$http.get('/favorites/search-keys')
                .then(response => {
                    this.searchKeys = response.data.searchKeys;
                });
        },

        addKeyWord: function(searchResults, shopId) {
            this.$http.get('/favorites/search-key/' + searchResults + '/' + shopId)
                .then(response => {
                    this.searchKeys.push(response.data.searchKey);
                });
        },

        searchByKeyWord: function (keyWord) {
            Bus.$emit('search-by-key-word', keyWord);
        }
    },

    mounted() {
        var that = this;
        Bus.$on('add-search-key', function (data) {
            that.addKeyWord(data.searchResults, data.id);
        });
        Bus.$on('is-search', function (search) {
            that.isSearch = search;
        });
        this.getSearchKeys();
    }
});