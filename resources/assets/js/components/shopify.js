Vue.component('shopify', {
    props: ['user'],

    data() {
        return {
            searchResults : '',
            bestSellers : [],
            caption: '',
            page: 1,
            iframeItem: false,
            iframeUrl: '',
            iDisplayStart: 0,
            iDisplayLength: 10,
            currentPage: 1,
            currentPages: [
                {page: 1},
                {page: 2},
                {page: 3},
                {page: 4},
                {page: 5}
            ],
            allPagesCount: 0,
            iSortingCols: 2,
            sSortDir_0: 'desc'
        };
    },

    methods: {
        search: function () {
            var data = {
                sSearch: this.searchResults,
                sEcho: 1,
                iColumns: 5,
                iDisplayStart: 0,
                iDisplayLength: 10,
                iSortCol_0: this.iSortingCols,
                sSortDir_0: this.sSortDir_0,
                iSortingCols: 1,
                bSortable_0: true,
                bSortable_1: true,
                bSortable_2: true,
                bSortable_3: true,
                bSortable_4: true,
            };
            this.$http.post('/shopify-best-sellers-search', data)
                .then(response => {
                    this.bestSellers = response.data.aaData;
                    this.allPagesCount = Math.ceil(response.data.iTotalDisplayRecords / this.iDisplayLength);
                    this.currentPage = 1;
                    this.currentPages = [{page: 1},{page: 2},{page: 3},{page: 4},{page: 5}];
                    if (this.allPagesCount < 5) {
                        var pagesArray = []
                        for (var i = 1; i <= this.allPagesCount; i++) {
                            pagesArray.push({page: i});
                        }
                        this.currentPages = pagesArray;
                    }
                });
        },

        changePage: function (page) {
            if ( page > 0 && page <= this.allPagesCount) {
                if (page > 3 && page < this.allPagesCount - 3) {
                    if (this.allPagesCount < 5) {
                        var pagesArray = []
                        for (var i = 1; i <= this.allPagesCount; i++) {
                            pagesArray.push({page: i});
                        }
                    } else {
                        pagesArray = [
                            {page: page - 2},
                            {page: page - 1},
                            {page: page},
                            {page: page + 1},
                            {page: page + 2}
                        ];
                    }
                    this.currentPages = pagesArray;
                } else if (page <= 3) {
                    if (this.allPagesCount < 5) {
                        var pagesArray = []
                        for (var i = 1; i <= this.allPagesCount; i++) {
                            pagesArray.push({page: i});
                        }
                    } else {
                        pagesArray = [
                            {page: 1},
                            {page: 2},
                            {page: 3},
                            {page: 4},
                            {page: 5}
                        ];
                    }
                    this.currentPages = pagesArray;
                }
                this.currentPage = page;
                this.iDisplayStart = (page - 1) * this.iDisplayLength;
                this.refreshData();
            }
        },

        refreshData: function () {
            var data = {
                sSearch: this.searchResults,
                sEcho: 1,
                iColumns: 5,
                iDisplayStart: this.iDisplayStart,
                iDisplayLength: this.iDisplayLength,
                iSortCol_0: this.iSortingCols,
                sSortDir_0: this.sSortDir_0,
                iSortingCols: 1,
                bSortable_0: true,
                bSortable_1: true,
                bSortable_2: true,
                bSortable_3: true,
                bSortable_4: true,
            };
            this.$http.post('/shopify-best-sellers-search', data)
                .then(response => {
                    this.bestSellers = response.data.aaData;
                    this.allPagesCount = Math.ceil(response.data.iTotalDisplayRecords / this.iDisplayLength);
                    if(response.data.iTotalDisplayRecords < this.iDisplayLength) {
                        this.currentPage = 1;
                    }
                    if (this.allPagesCount < 5) {
                        var pagesArray = []
                        for (var i = 1; i <= this.allPagesCount; i++) {
                            pagesArray.push({page: i});
                        }
                        this.currentPages = pagesArray;
                    }
                });
        },

        clearSearch: function () {
            this.searchResults = ''
        },

        openIframe: function (capt, url) {
            this.caption = '';
            this.iframeUrl = '';
            this.caption = capt;
            this.iframeUrl = url;
            this.iframeItem = true;
            $('#modal-iframe').modal('show');
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 11
            };
            Bus.$emit('add-search-key', data);
        },

        orderBy: function (col) {
            this.iSortingCols = col;
            if (this.sSortDir_0 == 'desc') {
                this.sSortDir_0 = 'asc';
            }else{
                this.sSortDir_0 = 'desc';
            }
            this.refreshData();
        }
    },

    computed: {
    },

    mounted() {
        Bus.$emit('is-search', true);
        this.search();
    }
});