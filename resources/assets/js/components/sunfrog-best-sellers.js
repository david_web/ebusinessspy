Vue.component('sunfrog-best-sellers', {
    props: ['user'],

    data() {
        return {
            isLoadingSites : false,
            tshirtProducts : [],
            sunFrogProducts : [],
            category: 'All',
            categories: [
                {value: 'All', name: 'All'},
                {value: 'Trending', name: 'Its Trending'},
                {value: 'Automotive', name: 'Automotive'},
                {value: 'Drinking', name: 'Drinking'},
                {value: 'Faith', name: 'Faith'},
                {value: 'Fitness', name: 'Fitness'},
                {value: 'Funny', name: 'Funny'},
                {value: 'Gamer', name: 'Gamer'},
                {value: 'Geek-Tech', name: 'Geek-Tech'},
                {value: 'Hobby', name: 'Hobby'},
                {value: 'Holidays', name: 'Holidays'},
                {value: 'Jobs', name: 'Jobs'},
                {value: 'LifeStyle', name: 'LifeStyle'},
                {value: 'Movies', name: 'Movies'},
                {value: 'Music', name: 'Music'},
                {value: 'Outdoor', name: 'Outdoor'},
                {value: 'Pets', name: 'Pets'},
                {value: 'Political', name: 'Political'},
                {value: 'Sports', name: 'Sports'},
                {value: 'States', name: 'States'},
                {value: 'TV Shows', name: 'TV Shows'},
                {value: 'Zombies', name: 'Zombies'}
            ],
        };
    },

    methods: {
        getData: function () {
                this.isLoadingSites = true;
                this.$http.get('/t-shirt-research/sunfrog-best-category')
                    .then(response => {
                        this.tshirtProducts = response.data;
                        this.isLoadingSites = false;
                    });
        },

        getDataByCategory: function () {
                this.isLoadingSites = true;
                this.$http.get('/t-shirt-research/sunfrog-best-category/' + this.category)
                    .then(response => {
                        this.tshirtProducts = response.data;
                        this.isLoadingSites = false;
                    });
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 10
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    computed: {

    },


    mounted() {
        this.getData();
    }
});