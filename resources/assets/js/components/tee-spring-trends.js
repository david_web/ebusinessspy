Vue.component('tee-spring-trends', {
    props: ['user'],

    data() {
        return {
            isLoadingSites : false,
            tshirtProducts : [],
            sunFrogProducts : [],
            category: 'all',
            categories: [
                {value: 'all', name: 'All'},
                {value: 'Beards', name: 'Beards'},
                {value: 'Camping', name: 'Camping'},
                {value: 'Cats', name: 'Cats'},
                {value: 'Coffee', name: 'Coffee'},
                {value: 'Dogs', name: 'Dogs'},
                {value: 'Engineers', name: 'Engineers'},
                {value: 'Firefighters', name: 'Firefighters'},
                {value: 'Fishing', name: 'Fishing'},
                {value: 'Horses', name: 'Horses'},
                {value: 'Houston Texans', name: 'Houston Texans'},
                {value: 'Indianapolis Colts', name: 'Indianapolis Colts'},
                {value: 'Mechanics', name: 'Mechanics'},
                {value: 'Motorcycles', name: 'Motorcycles'},
                {value: 'NFL All Teams', name: 'NFL All Teams'},
                {value: 'Nurses', name: 'Nurses'},
                {value: 'Pittsburgh Steelers', name: 'Pittsburgh Steelers'},
                {value: 'Running', name: 'Running'},
                {value: 'Second Amendment', name: 'Second Amendment'},
                {value: 'Tennessee Titans', name: 'Tennessee Titans'},
                {value: 'Washington Redskins', name: 'Washington Redskins'},
                {value: 'Welders', name: 'Welders'},
                {value: 'Wine', name: 'Wine'},
                {value: 'Yoga', name: 'Yoga'}
            ],
        };
    },

    methods: {
        getData: function () {
                this.isLoadingSites = true;
                this.$http.get('/t-shirt-research/best-tee-spring-trends')
                    .then(response => {
                        this.tshirtProducts = response.data;
                        this.isLoadingSites = false;
                    });
        },

        getDataByCategory: function () {
            console.log(this.category);
                this.isLoadingSites = true;
                this.$http.get('/t-shirt-research/best-tee-spring-trends/' + this.category)
                    .then(response => {
                        this.tshirtProducts = response.data;
                        this.isLoadingSites = false;
                    });
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 10
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    computed: {

    },


    mounted() {
        this.getData();
    }
});