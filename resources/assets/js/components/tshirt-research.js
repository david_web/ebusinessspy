Vue.component('tshirt-research', {
    props: ['user'],

    data() {
        return {
            isLoadingSites : false,
            tshirtProducts : [],
            searchResults : '',
            searchResultsForMore : '',
            seen: false,
            page: 1,
            searchKeys: [],
            tabId: 1,
            shop: 'images'
        };
    },

    methods: {
        search: function () {
            this.seen = false;
            if (this.searchResults != ''){
                this.isLoadingSites = true;
                this.$http.get('t-shirt-research/' + this.shop + '/' + this.searchResults + '/' + this.page)
                    .then(response => {
                        this.searchResultsForMore = this.searchResults
                        this.tshirtProducts = response.data;
                        this.searchKeys = response.data.searchKeys;
                        this.isLoadingSites = false;
                        if (response.data.length == 0) {
                            this.seen = false;
                        } else {
                            this.seen = true;
                        }
                        
                    });
            } else {
                alert('Research field is required!')
            }
        },

        clearSearch: function () {
            this.searchResults = '';
        },

        loadTshirtMore: function () {
            this.page += 1;
            this.$http.get('t-shirt-research/' + this.shop + '/' + this.searchResultsForMore + '/' + this.page)
                .then(response => {
                    this.tshirtProducts = this.tshirtProducts.concat(response.data);
                    this.isLoadingSites = false;
                    this.seen = true;
                });
        },

        searchByKeyWord: function(key_word) {
            this.searchResults = key_word;
            this.search();
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 10
            };
            Bus.$emit('add-search-key', data);
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 10
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        },

        changeShop: function (tabId, shopName) {
            this.tabId = tabId;
            this.shop = shopName;
            this.page = 1;
            if (this.searchResults != ''){
                this.tshirtProducts = [];
                this.search();
            } else {
                this.tshirtProducts = [];
                this.seen = false;
            }
        }
    },

    computed: {
        pinterestUrl: function (argument) {
            return 'https://www.pinterest.com/search/pins/?q=' + this.searchResults;
        }
    },


    mounted() {
        var that = this;
        Bus.$on('search-by-key-word', function (keyWord) {
            that.searchResults = keyWord;
            that.search();
        });
        Bus.$emit('is-search', true);
    }
});