Vue.component('wanelo', {
    props: ['user'],

    data() {
        return {
            products : [],
            searchResults : 'undefined',
            categorySearch : 'all',
            sortSearch : 'undefined',
            page: 1,
            seen: false,
            searchKeys: [],
            sorts: [
                {text: 'Order by ...', value: 'undefined'},
                {text: 'by Save Count: Low To High', value: 'save_asc'},
                {text: 'by Save Count:  High To Low', value: 'save_desc'}
            ],
            categories: [
                { text: 'All categories', value: 'all' },
                { text: 'Active', value: 'active' },
                { text: 'Bohemian & Rustic', value: 'bohemian' },
                { text: 'Classic', value: 'classic' },
                { text: 'Fantasy', value: 'fantasy' },
                { text: 'High Fashion', value: 'high_fashion_couture' },
                { text: 'Minimalist', value: 'minimalist' },
                { text: 'Soft Grunge', value: 'soft_grunge' },
                { text: 'Surf & Skate', value: 'surf_skate' },
                { text: 'Trendy', value: 'trendy' },
                { text: 'Urban', value: 'urban' },
                { text: 'Vintage & Retro', value: 'vintage_retro' },
            ],
            error: '',
            errorShow: false
        };
    },

    methods: {
        search: function () {
            // if (this.searchResults == '') {
            //     this.errorShow = true;
            //     this.error = 'Search parametr is required!';
            // } else {
                this.errorShow = false;
                this.$http.get('/wanelo-search/' + this.categorySearch + '/' + this.sortSearch + '/' + this.searchResults)
                    .then(response => {
                        this.products = response.data.waneloProducts;
                        this.searchKeys = response.data.searchKeys;
                        this.seen = true;
                    });
            // }
        },

        clearSearch: function () {
            this.searchResults = '';
        },

        loadMore: function () {
            this.page += 1;
            this.$http.get('/wanelo-search/' + this.categorySearch + '/' + this.sortSearch + '/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.products = this.products.concat(response.data.waneloProducts);

                    this.seen = true;
                });
        },

        searchByKeyWord: function(key_word) {
            this.searchResults = key_word;
            this.search();
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 4
            };
            Bus.$emit('add-search-key', data);
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 4
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        }
    },

    mounted() {
        var that = this;
        Bus.$on('search-by-key-word', function (keyWord) {
            that.searchResults = keyWord;
            that.search();
        });
        Bus.$emit('is-search', true);
        this.search();
    }
});