Vue.component('youtube-popular', {
    props: ['user'],

    data() {
        return {
            youtubeProducts : [],
            caption: '',
            description: '',
            infoItem: false,
            selectedVideoUrl: '',
            selectedVideoCaption: '',
            videoItem: false,
        };
    },

    methods: {
        search: function () {
            this.loading = true;
            this.$http.get('youtube-most-popular-data')
                .then(response => {
                    this.youtubeProducts = response.data.data;
                    this.page = response.data.info;
                    this.loading = false;
                    this.seen = true;
                });
        },

        infoModal: function (capt, desc) {
            this.caption = capt;
            this.description = desc;
            this.infoItem = true;
            $('#modal-info').modal('show');
        },

        videoModal: function (capt, url) {
            this.selectedVideoCaption = capt;
            this.selectedVideoUrl = url;
            this.videoItem = true;
            $('#modal-video').modal('show');
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 6
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        },

        clearIframe: function () {
            this.selectedVideoUrl = '';
        }
    },

    mounted() {
        this.search();
    }
});