Vue.component('youtube-videos', {
    props: ['user'],

    data() {
        return {
            searchResults : 'most watched',
            youtubeProducts : [],
            caption: '',
            description: '',
            infoItem: false,
            selectedVideoUrl: '',
            selectedVideoCaption: '',
            videoItem: false,
            page: 'start',
            seen: false,
            commonVid: 'no'
        };
    },

    methods: {
        search: function () {
            this.loading = true;
            this.$http.get('/youtube-video-search/' + 0 + '/' + this.commonVid + '/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.youtubeProducts = response.data.data;
                    this.page = response.data.info;
                    this.loading = false;
                    this.seen = true;
                });
        },

        loadYoutubeMore: function () {
            this.commonVid = 'yes';
            this.$http.get('/youtube-video-search/' + 0 + '/' + this.commonVid + '/' + this.searchResults + '/' + this.page)
                .then(response => {
                    this.youtubeProducts = this.youtubeProducts.concat(response.data.data);
                    this.page = response.data.info;
                    this.loading = false;
                    this.seen = true;
                });
        },

        clearSearch: function () {
            this.searchResults = ''
        },

        infoModal: function (capt, desc) {
            this.caption = capt;
            this.description = desc;
            this.infoItem = true;
            $('#modal-info').modal('show');
        },

        videoModal: function (capt, url) {
            this.selectedVideoCaption = capt;
            this.selectedVideoUrl = url;
            this.videoItem = true;
            $('#modal-video').modal('show');
        },

        addFavoriteItem: function (img, url) {
            var data = {
                img: img,
                url: url,
                shop_id: 6
            };
            this.$http.post('/add-favorite-items', data)
                .then(response => {
                });
        },

        addSearchKey: function () {
            var data = {
                searchResults: this.searchResults,
                id: 6
            };
            Bus.$emit('add-search-key', data);
        },

        clearIframe: function () {
            this.selectedVideoUrl = '';
        }
    },

    mounted() {
        Bus.$emit('is-search', true);
        this.search();
    }
});