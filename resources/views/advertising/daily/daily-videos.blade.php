@extends('home')

@section('cont')

<daily-motion-videos :user="user" inline-template>
<div class="row">
    <div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
        <div class="panel-heading">Daily Motion videos</div>
        <div class="panel-body">
            <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
                <!-- Search Field -->
                <div class="form-group">
                    <div class="col-md-9">
                        <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                    </div>
                    <div class="col-md-3" style="padding-left: 0">
                        <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                        <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                        <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4" v-for="item in dailyProducts">
                    <div class="panel panel-default">
                        <div class="panel-body" style="height: 350px;">
                            <a v-bind:href="item.url" target="blank"><img v-bind:src="item.thumbnail_url" style="width: 100%;"></a>
                            <div><b>@{{ item.title }}</b></div>
                            <div>@{{ item.chanel }}</div>
                            <div>Duration: @{{ item.duration }}</div>
                            <div><i class="fa fa-eye"></i> @{{ item.views_total }} views</div>
                                <a class="btn btn-primary" @click="addFavoriteItem( item.thumbnail_url, item.url )"><i class="fa fa-heart"></i></a>
                                <a class="btn btn-primary" @click="infoModal( item.title, item.description )"><i class="fa fa-external-link" ></i> View Info</a>
                        </div>
                    </div>
                </div>
            </div>
            <a class="btn-block btn btn-info col-xs-12" v-if="seen" @click="loadDailyMore()">Load More</a>
        </div>
    </div>


    <!-- Info Modal -->
    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog">
        <div class="modal-dialog" v-show="infoItem">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        @{{ caption }}
                    </h4>
                </div>

                <div class="modal-body" v-if="description != ''">
                    @{{ description }}
                </div>
                <div class="modal-body" v-else>
                    There are no information.
                </div>

                <!-- Modal Actions -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</daily-motion-videos>

@endsection