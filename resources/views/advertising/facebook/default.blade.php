@extends('home')
@section('cont')
<facebook-videos :user="user" inline-template>
  <div class="row">
  	<div class="col-md-12">
      <div class="panel panel-piluku" style="min-height:300px;">
        <div class="panel-body">
          <div style="text-align:center">
            <br />
              <br />
              <br />
              <h4>Please login with your Facebook to search for videos.</h4><br>
              <a class="btn btn btn-primary btn-lg" href="{{ $viewData['login_url'] }}"><i class="fa fa-facebook"></i> LOGIN WITH FACEBOOK</a>
          </div> 
        </div>
      </div>
    </div>
  	
  </div>
</facebook-videos>
@endsection