@extends('home')
@section('cont')
<facebook-ads :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">HOT Facebook Ad Search</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-12">
                    <label for="keyword">Keyword</label>
                    <input type="text" id="keyword" class="form-control" v-model="keyWord">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="website">Website</label>
                    <input type="text" id="website" class="form-control" v-model="website">
                </div>
            </div>
            <div class="form-group" style="font-size: 13px">
                <div class="col-md-12">
                    <label class="radio-inline">
                        <input type="radio" value="just pay shipping" name="website" v-model="website">
                        just pay shipping
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="http://teespring.com" name="website" v-model="website">
                        teespring.com
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="http://gearbubble.com" name="website" v-model="website">
                        gearbubble.com
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="http://viralstyle.com" name="website" v-model="website">
                        viralstyle.com
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="http://represent.com" name="website" v-model="website">
                        represent.com
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="http://sunfrog.com" name="website" v-model="website">
                        sunfrog.com
                    </label>
                </div>
            </div>
            <div class="form-group" style="font-size: 13px">
                <div class="col-md-12">
                    <span><b>Post type:</b></span>
                    <label class="radio-inline">
                        <input type="radio" value="recent" name="postType" v-model="postType">
                        Recent
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="all" name="postType" v-model="postType">
                        All
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="live" name="postType" v-model="postType">
                        Live
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="pages" name="postType" v-model="postType">
                        Pages
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="shared" name="postType" v-model="postType">
                        Shared
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="photos" name="postType" v-model="postType">
                        Photos
                    </label>
                    <label class="radio-inline">
                        <input type="radio" value="videos" name="postType" v-model="postType">
                        Videos
                    </label>
                </div>
            </div>
            <div class="form-group" style="font-size: 13px" v-show=" postType == 'recent'">
                <div class="col-md-12">
                    <span><b>Time Period:</b></span>
                    <label class="radio-inline">
                        <input type="radio" v-model="timePeriod" value="this-week" name="timePeriod">
                        This Week
                    </label>
                    <label class="radio-inline">
                        <input type="radio" v-model="timePeriod" value="this-month" name="timePeriod">
                        This Month
                    </label>
                    <label class="radio-inline">
                        <input type="radio" v-model="timePeriod" value="last-week" name="timePeriod">
                        Last Week
                    </label>
                    <label class="radio-inline">
                        <input type="radio" v-model="timePeriod" value="last-month" name="timePeriod">
                        Last Month
                    </label>
                </div>
            </div>
            <!-- <div class="col-md-9"></div> -->
            <div style="text-align: right;">
                <a @click="search()" :href=" computedUrl" target="blank" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i> Search posts</a>
            </div>
        </form>
    </div>
</div>
</div>
</facebook-ads>
@endsection