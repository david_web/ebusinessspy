@extends('home')
@section('cont')
<facebook-videos :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">Facebook videos</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-9">
                    <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                </div>
                <div class="col-md-3" style="padding-left: 0">
                    <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                    <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                    <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4" v-for="item in fbProducts">
                <div class="panel panel-default" style="min-height: 220px">
                    <div class="panel-body" style="text-align: center;">
                        <img v-bind:src="item.picture.data.url" style="border-radius: 50%; margin-bottom:10px; height: 50px">
                        <div style="color: #6b9dbb;margin-bottom: 10px"><a :href="'https://www.facebook.com/' + item.id" target="blank">@{{ item.name }}</a></div>
                        <div style="margin-bottom: 10px"><b>Category: @{{ item.category }}</b></div>
                        <!-- <div v-if="item.likes">Likes: </div> -->
                        <div class="btn-group">
                            <a class="btn btn-info" @click = "addFavoriteItem( item.picture.data.url, 'https://www.facebook.com/' + item.id)"><i class="fa fa-heart"></i></a>
                            <a class="btn btn-info" @click="getTopPosts( item.id, 128, item.name )"><i class="fa fa-file-video-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Modal -->
    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog">
        <div class="modal-dialog" v-show="topItem" style="width: 960px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        @{{ topName }}
                    </h4>
                </div>

                <div class="modal-body">
                    <table class="table" v-if="topFbPosts.length != 0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Top Story</th>
                                <th>Links</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item in topFbPosts">
                                <td></td>
                                <td style="text-align: left;">
                                    <img :src="item.picture" class="img-thumbnail" style="max-height: 150px">
                                </td>
                                <td style="width: 50%">
                                    <div>
                                        @{{ item.description }}
                                    </div>
                                    <div><b>
                                        <i class="fa fa-thumbs-o-up"></i> @{{ item.likes.summary.total_count }} likes • <i class="fa fa-comments"></i> @{{ item.comments.summary.total_count }} comments
                                    </b></div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                      <a :href="'https://www.facebook.com/' + item.id" class="btn btn-info" style="text-align: left;"><i class="fa fa-facebook-official"></i> View in Facebook</a>
                                      <a :href="item.source" target="blank" class="btn btn-info" style="text-align: left;"><i class="fa fa-file-video-o"></i> View Video</a>
                                      <a :href="item.picture" target="blank" class="btn btn-info" style="text-align: left;"><i class="fa fa-picture-o"></i> View Picture</a>
                                      <a @click = "addFavoriteItem( item.picture, 'https://www.facebook.com/' + item.id)" class="btn btn-info" style="text-align: left;"><i class="fa fa-heart"></i> Add to library</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div v-else>Facebook did not return any data.</div>
                </div>

                <!-- Modal Actions -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</facebook-videos>
@endsection