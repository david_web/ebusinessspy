@extends('home')

@section('cont')

<shopify :user="user" inline-template>
<div class="row">
    <div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
        <div class="panel-heading">Shopify Best Sellers</div>
        <div class="panel-body">
            <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
                <!-- Search Field -->
                <div class="form-group">
                    <div class="col-md-9">
                        <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                    </div>
                    <div class="col-md-3" style="padding-left: 0">
                        <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                        <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                        <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                    </div>
                </div>
            </form>
        </div>
        <div class="panel-body">
            Show
            <select name="iDisplayLength" @change="refreshData()" v-model="iDisplayLength">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
            entries
            <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th @click="orderBy(0)" style="cursor: pointer;">
                                Web Site <i class="fa fa-sort"></i>
                            </th>
                            <th @click="orderBy(1)" style="cursor: pointer;">
                                Global Rank <i class="fa fa-sort"></i>
                            </th>
                            <th @click="orderBy(2)" style="cursor: pointer;">
                                Traffic Volume <i class="fa fa-sort"></i>
                            </th>
                            <th>
                                Best Sellers
                            </th>
                            <th>
                                Traffic Stats
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="bestSeller in bestSellers">
                            <td><a v-bind:href="'http://'+bestSeller.webSite" target="_blank">@{{ bestSeller.webSite }}</a></td>
                            <td>@{{ bestSeller.globalRank }}</td>
                            <td>@{{ bestSeller.trafficVolume }}</td>
                            <td>
                                <a @click="openIframe( bestSeller.webSite, bestSeller.bestSellers)" style="cursor: pointer;">
                                    View Best Sellers
                                    <i class="fa fa-tv"></i>
                                </a>
                            </td>
                            <td>
                                <a @click="openIframe( bestSeller.webSite, bestSeller.bestSellers)" class="btn btn-md btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="btn btn-md btn-info">
                                    <i class="fa fa-bar-chart"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: center;" v-show="!(allPagesCount <= 1) ">
                <ul class="pagination">
                    <li @click="changePage( currentPage - 1 )" style="cursor: pointer" :class="[currentPage == 1 ? 'disabled' : '']"><a>←</a></li>
                    <li v-for="item in currentPages" @click="changePage( item.page )" :class="[currentPage == item.page ? 'active' : '']" style="cursor: pointer;">
                        <a>@{{ item.page }}</a>
                    </li>
                    <li
                        @click="changePage( currentPage + 1 )"
                        style="cursor: pointer"
                        :class="[currentPage == allPagesCount ? 'disabled' : '']"
                    >
                        <a>→</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <!-- Info Modal -->
    <div class="modal fade" id="modal-iframe" tabindex="-1" role="dialog">
        <div class="modal-dialog" v-show="iframeItem" style="width: 960px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        @{{ caption }}
                    </h4>
                </div>

                <div class="modal-body" style="height: 800px">
                        <iframe :src="iframeUrl" style="width: 100%; border: none; height: 100%"></iframe>
                </div>

                <!-- Modal Actions -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</shopify>

@endsection