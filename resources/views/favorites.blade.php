@extends('home')
@section('cont')
<favorites :user="user" inline-template>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">Favorites</div>
        <div class="panel-body">
            <div class="btn-group" style="margin-bottom: 15px">
                <div class="btn btn-default" v-bind:class="[page == 'media' ? 'page-selected' : '']" @click="changePage('media')">Media</div>
                <div class="btn btn-default" v-bind:class="[page == 'search' ? 'page-selected' : '']" @click="changePage('search')">Search keys</div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-group" v-if="page=='search'">
                        <a class="list-group-item" v-for="shop in shops" v-if="shop.search_key.length != 0" @click="changeShop(shop.id)" v-bind:class="[shopId == shop.id ? 'disabled' : '']" style="cursor: pointer">@{{ shop.name }}</a>
                    </ul>
                    <ul class="list-group" v-if="page=='media'">
                        <a class="list-group-item" v-for="shop in shops" v-if="shop.favorite_items.length != 0" @click="changeShop(shop.id)" v-bind:class="[shopId == shop.id ? 'disabled' : '']" style="cursor: pointer">@{{ shop.name }}</a>
                    </ul>
                </div>
                <div class="col-md-8">
                    <ul class="list-group" v-if="page=='search'">
                        <li class="list-group-item" style="overflow: auto; padding: 7px" v-for="searchKey in shops[shopId].search_key">
                            <p style="margin: 4px; float: left">@{{ searchKey.key_word }}</p> <a class="btn btn-danger" style="float: right" @click="deleteKeyWord(searchKey.id)" role="button" title="Delete"><i class="fa fa-trash"></i></a>
                        </li>
                    </ul>

                    <div v-if="page=='media' && shops[shopId]" class="col-md-12">
                        <div v-for="item in shops[shopId].favorite_items">
                            <div class="col-md-4 favorites-item" v-on:mouseover="mouseOver(item.id)" v-on:mouseout="mouseUp(item.id)">
                                <a v-bind:href="item.url" target="_blank" style="position: relative;">
                                    <img v-bind:class="[active[item.id] ? 'opacity-img' : '']" height="144px" class="thumbnail col-xs-12" id="image_0" v-bind:src="item.img">
                                </a>
                                <div style="position: absolute; bottom: 30px; left: 50px;" v-show="active[item.id]">
                                    <a class="btn btn-danger" @click="deleteItem(item.id)" role="button" title="Delete"><i class="fa fa-trash"></i>
                                    <a class="btn btn-info" v-bind:href="item.url" target="_blank" title="View Item"><i class="fa fa-external-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</favorites>
@endsection
@section('scripts')
<style>
    .page-selected {
        background-color: #ccc
    }
    .page-selected:hover {
        background-color: #ccc
    }
    .opacity-img {
        opacity: 0.4;
        filter: alpha(opacity=40);
    }
</style>
@endsection