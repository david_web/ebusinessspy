@extends('spark::layouts.app')

@section('content')
<link rel="stylesheet" href="/css/pace-theme-minimal.css" />
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default panel-flush">
                    <div class="panel-heading">
                        Menu
                    </div>

                    <div class="panel-body">
                        <div class="spark-settings-tabs">
                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                @if (isset($shopifyBestSellers))
                                <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;">
                                @else
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2;">
                                @endif
                                    <a href="{{action('ShopifyController@getShopifyBestSellers')}}">
                                        <i class="fa fa-fw fa-btn fa-shopping-cart"></i>Shopify Best Sellers
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <li data-toggle="collapse" data-target="#tshirt"  role="presentation" style="cursor: pointer;border-bottom: 1px solid #e4ecf2;" aria-expanded="true">
                                    <a>
                                        <i class="fa fa-user"></i> T-Shirt research
                                        <span style="float:right"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                            </ul>
                            @if (isset($tShirt))
                            <ul id="tshirt" class="sub-menu collapse nav spark-settings-stacked-tabs in" role="tablist">
                            @else
                            <ul id="tshirt" class="sub-menu collapse nav spark-settings-stacked-tabs" role="tablist">
                            @endif
                                @if (isset($tShirtSpy))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{ action('TshirtController@getTshirtResearch') }}">
                                        <b>•</b> T-Shirt Spy
                                    </a>
                                </li>

                                @if (isset($sunFrog))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{action('TshirtController@getSunFrogBestPage')}}">
                                        <b>•</b> SunFrog Best Sellers
                                    </a>
                                </li>

                                @if (isset($teeSpring))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{action('TshirtController@getTeeSpringTrends')}}">
                                        <b>•</b> Tee Spring Trends
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <li data-toggle="collapse" data-target="#shops"  role="presentation" style="cursor: pointer;border-bottom: 1px solid #e4ecf2;">
                                    <a>
                                        <i class="fa fa-exchange"></i> Ecommerce Research
                                        <span style="float:right"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                            </ul>
                            @if (isset($ecommerce))
                            <ul id="shops" class="sub-menu collapse nav spark-settings-stacked-tabs in" role="tablist">
                            @else
                            <ul id="shops" class="sub-menu collapse nav spark-settings-stacked-tabs" role="tablist">
                            @endif
                                @if (isset($ebay))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{action('ShopsController@getEbay')}}">
                                        <i class="fa fa-fw fa-btn fa-bullhorn"></i>Ebay Spy
                                    </a>
                                </li>

                                @if (isset($aliExpress))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{action('ShopsController@getAliExpress')}}">
                                        <i class="fa fa-fw fa-btn fa-bar-chart"></i>Ali Express Spy
                                    </a>
                                </li>

                                @if (isset($amazon))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{action('ShopsController@getAmazon')}}">
                                        <i class="fa fa-fw fa-btn fa-amazon"></i>Amazon Spy
                                    </a>
                                </li>

                                @if (isset($wanelo))
                                <li role="presentation" class="active">
                                @else
                                <li role="presentation">
                                @endif
                                    <a href="{{action('ShopsController@getWanelo')}}">
                                        <i class="fa fa-fw fa-btn fa-shopping-bag"></i>Wanelo Spy
                                    </a>
                                </li>

                                @if (isset($etsy))
                                <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;">
                                @else
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2;">
                                @endif
                                    <a href="{{action('ShopsController@getEtsy')}}">
                                        <i class="fa fa-fw fa-btn fa-shopping-basket"></i>Etsy Spy
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <li data-toggle="collapse" data-target="#advertising"  role="presentation" style="cursor: pointer;border-bottom: 1px solid #e4ecf2;">
                                    <a>
                                        <i class="fa fa-buysellads"></i> Advertising Tools
                                        <span style="float:right"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                            </ul>
                            @if (isset($advertising))
                            <ul id="advertising" class="sub-menu collapse nav spark-settings-stacked-tabs in" role="tablist">
                            @else
                            <ul id="advertising" class="sub-menu collapse nav spark-settings-stacked-tabs" role="tablist">
                            @endif
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2; cursor: pointer;" data-toggle="collapse" data-target="#facebook"  >
                                    <a>
                                        <i class="fa fa-fw fa-btn fa-facebook"></i>FB Tools
                                        <span style="float:right"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                                @if (isset($facebook))
                                <ul id="facebook" class="sub-menu collapse nav spark-settings-stacked-tabs in" role="tablist">
                                @else
                                <ul id="facebook" class="sub-menu collapse nav spark-settings-stacked-tabs" role="tablist">
                                @endif
                                    @if (isset($fbVideos))
                                    <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @else
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @endif
                                        <a href="{{ action('AdvertisingController@getFbVideos') }}" style="padding-left: 30px">
                                            <b>•</b> HOT FB Videos
                                        </a>
                                    </li>
                                    @if (isset($fbSearch))
                                    <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @else
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @endif
                                        <a href="{{ action('AdvertisingController@getFbAds') }}" style="padding-left: 30px">
                                            <b>•</b> HOT FB Ad Search
                                        </a>
                                    </li>
                                </ul>
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2; cursor: pointer;" data-toggle="collapse" data-target="#youtube"  >
                                    <a>
                                        <i class="fa fa-fw fa-btn fa-youtube"></i>Youtube
                                        <span style="float:right"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                                @if (isset($youtube))
                                <ul id="youtube" class="sub-menu collapse nav spark-settings-stacked-tabs in" role="tablist">
                                @else
                                <ul id="youtube" class="sub-menu collapse nav spark-settings-stacked-tabs" role="tablist">
                                @endif
                                    @if (isset($youtubeHot))
                                    <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @else
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @endif
                                        <a href="{{ action('AdvertisingController@getHotYoutube') }}" style="padding-left: 30px">
                                            <b>•</b> HOT Video Search
                                        </a>
                                    </li>
                                    @if (isset($youtubeMostPopular))
                                    <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @else
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                    @endif
                                        <a href="{{ action('AdvertisingController@getYoutubeMostPopular') }}" style="padding-left: 30px">
                                            <b>•</b> View Most Popular
                                        </a>
                                    </li>
                                </ul>
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2; cursor: pointer;" data-toggle="collapse" data-target="#pinterest"  >
                                    <a>
                                        <i class="fa fa-fw fa-btn fa-pinterest"></i>Pinterest
                                        <span style="float:right"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                    </a>
                                </li>
                                <ul class="sub-menu collapse nav spark-settings-stacked-tabs" role="tablist" id="pinterest">
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                        <a href="https://www.pinterest.com/search/?q=teespring.com" style="padding-left: 30px" target="blank">
                                            <b>•</b> teespring
                                        </a>
                                    </li>
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                        <a href="https://www.pinterest.com/search/pins/?q=teespring.com" style="padding-left: 30px" target="blank">
                                            <b>•</b> pins teespring
                                        </a>
                                    </li>
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                        <a href="https://www.pinterest.com/search/pins/?q=viralstyle.com" style="padding-left: 30px" target="blank">
                                            <b>•</b> viralstyle
                                        </a>
                                    </li>
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                        <a href="https://www.pinterest.com/search/pins/?q=gearbubble.com" style="padding-left: 30px" target="blank">
                                            <b>•</b> gearbubble
                                        </a>
                                    </li>
                                    <li role="presentation" style="border-bottom: 1px solid #e4ecf2;font-size: 13px">
                                        <a href="https://www.pinterest.com/search/pins/?q=cafepress.com" style="padding-left: 30px" target="blank">
                                            <b>•</b> cafepress
                                        </a>
                                    </li>
                                </ul>
                                @if (isset($vimeo))
                                <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;">
                                @else
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2;">
                                @endif
                                    <a href="{{ action('AdvertisingController@getHotVimeo') }}">
                                        <i class="fa fa-fw fa-btn fa-vimeo"></i></i>Vimeo Videos
                                    </a>
                                </li>
                                @if (isset($dailyMotion))
                                <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;">
                                @else
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2;">
                                @endif
                                    <a href="{{ action('AdvertisingController@getHotDaily') }}">
                                        <i class="fa fa-fw fa-btn fa-chevron-right"></i></i>Daily Motion Videos
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                @if (isset($favorites))
                                <li role="presentation" class="active" style="border-bottom: 1px solid #e4ecf2;">
                                @else
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2;">
                                @endif
                                    <a href="{{action('FavoritesController@getFavorites')}}">
                                        <i class="fa fa-fw fa-btn fa-heart"></i>Favorites
                                    </a>
                                </li>
                            </ul>

                            <ul class="nav spark-settings-stacked-tabs" role="tablist">
                                <li role="presentation" style="border-bottom: 1px solid #e4ecf2; cursor: pointer" @click="contactUsModal()">
                                    <a>
                                        <i class="fa fa-fw fa-btn fa-comment"></i>Contact Us
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <search-keys inline-template>
                    <div class="panel panel-default " style="padding: 0" v-if="isSearch">
                        <div class="panel-heading">Search keys</div>
                        <div class="panel-body">
                            <ul class="list-group" v-if="searchKeys != []">
                                <li class="list-group-item"
                                    v-for="searchKey in searchKeys"
                                    style="cursor: pointer;"
                                    @click="searchByKeyWord(searchKey.key_word)">
                                    <span>
                                        @{{searchKey.key_word}}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </search-keys>
            </div>
            <div class="col-md-9">
                @yield('cont', '')
            </div>
        </div>
        <!-- Contact Us Modal -->
        <div class="modal fade" id="modal-contact-us" tabindex="-1" role="dialog">
            <div class="modal-dialog" v-show="contactItem">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">
                            Contact Us. <b style="color:#6cadd1">Support Email: support@ebusinessspy.com</b>
                        </h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="type">Type:</label>
                            <select class="form-control" id="type" v-model="emailType">
                              <option value="General Inquiry">General Inquiry</option>
                              <option value="Technical Inquiry">Technical Inquiry</option>  
                              <option value="Feature Request">Feature Request</option>
                              <option value="Bug Report">Bug Report</option>
                              <option value="Billing Inquiry">Billing Inquiry</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject:</label>
                            <input type="text" class="form-control" id="subject" v-model="emailSubject">
                        </div>
                        <div class="form-group">
                            <label for="message">Message:</label>
                            <textarea class="form-control" v-model="emailMessage" id="message" style="resize: vertical; height: 120px"></textarea>
                        </div>
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer" >
                        <button class="btn btn-primary" @click="submitMessage()">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>
@endsection
