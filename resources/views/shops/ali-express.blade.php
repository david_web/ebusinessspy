@extends('home')
@section('cont')
<ali-express :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">Ali Express Spy</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <div v-if="errorShow" class="alert alert-danger">@{{ error }}</div>
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-9">
                    <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                </div>
                <div class="col-md-3" style="padding-left: 0">
                    <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                    <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                    <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category:</label>
                        <div class="col-sm-9">
                            <select v-model="categorySearch" class="form-control">
                                <option v-for="category in categories" v-bind:value="category.value">
                                    @{{ category.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sort:</label>
                        <div class="col-sm-9">
                            <select v-model="sortSearch" class="form-control">
                                <option v-for="sort in sorts" v-bind:value="sort.value">
                                    @{{ sort.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-md-4" v-for="item in aliExpressProducts">
                <div class="panel panel-default" style="padding: 10px; height: 450px;">
                    <img v-bind:src="item.thumbUrl" class="img-rounded" alt="Cinque Terre" style="margin-bottom: 10px; width: 100%">
                    <p v-html="item.productTitle"></p>
                    <div style="">
                        <b>Price: </b><del>$@{{ item.price }}</del> $@{{ item.salePrice }}<span>/ piece</span>
                    </div>
                    <div style="">
                        <a role="button" title="Save" class="btn btn-primary" @click="addFavoriteItem(item.thumbUrl, item.productUrl)"><i class="fa fa-heart"></i></a>
                        <a v-bind:href="item.productUrl" target="_blank" class="btn btn-info"><i class="fa fa-external-link"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-block btn btn-info col-xs-12" v-if="seen" @click="loadAliExpressMore()">Load More</a>
    </div>
</div>
</div>
</ali-express>
@endsection