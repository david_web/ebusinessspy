@extends('home')
@section('cont')
<amazon :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">Amazon Spy</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-9">
                    <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                </div>
                <div class="col-md-3" style="padding-left: 0">
                    <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                    <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                    <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="row" v-for="item in amazonProducts">
            <div class="col-xs-2">
                <a>
                    <img class="thumbnail col-xs-12" id="image_0" v-bind:src="item.url">
                </a>
            </div>
            <div class="col-xs-10">
                <h4>
                    <a target="_blank" id="url_0" v-bind:href="item.DetailPageURL">
                    <span id="caption_0">@{{item.Title}}</span>
                    </a>
                </h4>
                
                <h5>Price: <span id="price_0">@{{item.Price}}</span></h5>

                <div>
                    <h5 class="ng-scope">Description:</h5>
                    <h5><span id="description_0" v-html="item.Content"></span></h5>
                </div>
                <br>
                <div class="btn-group btn-group-justified">
                    
                    <div>
                        <a class="btn btn-primary" role="button" title="Save" @click="addFavoriteItem(item.url, item.DetailPageURL)"><i class="fa fa-heart"></i></a>

                        <a class="btn btn-info" v-bind:href="item.DetailPageURL" target="_blank"><i class="fa fa-external-link"></i> View Item</a>

                        <button @click="reviewModal(item.IFrameURL)" class="btn btn-default"><i class="fa fa-eye"></i> Reviews</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn-block btn btn-info col-xs-12" v-if="seen" @click="loadAmazonMore()">Load More</a>

    <!-- Reviews Modal -->
    <div class="modal fade" id="modal-reviews" tabindex="-1" role="dialog">
        <div class="modal-dialog" v-show="reviewItem">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        Reviews
                    </h4>
                </div>

                <div class="modal-body" style="padding: 0;">
                    <iframe v-bind:src="reviewItemIframe" style="width: 100%;height: 400px;border: none;"></iframe>
                </div>

                <!-- Modal Actions -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</amazon>
@endsection