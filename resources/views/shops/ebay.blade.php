@extends('home')
@section('cont')
<ebay :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">eBay Spy</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-9">
                    <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                </div>
                <div class="col-md-3" style="padding-left: 0">
                    <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                    <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                    <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category:</label>
                        <div class="col-sm-9">
                            <select v-model="categorySearch" class="form-control">
                                <option v-for="category in categories" v-bind:value="category.value">
                                    @{{ category.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sort:</label>
                        <div class="col-sm-9">
                            <select v-model="sortSearch" class="form-control">
                                <option v-for="sort in sorts" v-bind:value="sort.value">
                                    @{{ sort.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Order:</label>
                        <div class="col-sm-9">
                            <select v-model="orderSearch" class="form-control">
                                <option v-for="order in orders" v-bind:value="order.value">
                                    @{{ order.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="panel-body">
        <div class="row" v-for="item in ebayProducts">
            <div class="col-xs-3">
                    <img v-bind:id="item.id" style="max-width: 140px" v-bind:src="item.thumbUrl">
            </div>
            <div class="col-xs-9">
                <div class="pull-right">
                    <h4><span class="label label-info"> Past sales:  @{{item.pastSales}}</span></h4>
                </div>

                <h4><span class="label label-warning"> Watch count:  @{{item.watchCount}}</span></h4>

                <a target="_blank" v-bind:id="item.id" v-bind:href="item.viewItemURL">
                    <h4 v-bind:id="item.id" class="ng-binding">@{{item.caption}}</h4>
                </a>

                <div v-bind:id="item.id" class="ng-binding"></div>

                <h5>Current Bid/Price: <strong v-bind:id="item.id">$@{{item.price}}</strong></h5>
                <h5>Time left: <strong> @{{item.timeLeft}}</strong></h5>
                <div class="pull-right">
                    <a class="btn btn-primary btn-xs" role="button" title="Save" @click="addFavoriteItem(item.thumbUrl, item.viewItemURL)"><i class="fa fa-heart"></i></a>
                    <a target="_blank" class="btn btn-info btn-xs" v-bind:href="item.viewItemURL"><i class="fa fa-external-link"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</ebay>
@endsection