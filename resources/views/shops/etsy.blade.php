@extends('home')
@section('cont')
<etsy :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">Etsy Spy</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-9">
                    <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                </div>
                <div class="col-md-3" style="padding-left: 0">
                    <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                    <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                    <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category:</label>
                        <div class="col-sm-9">
                            <select v-model="categorySearch" class="form-control">
                                <option v-for="category in categories" v-bind:value="category.value">
                                    @{{ category.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sort:</label>
                        <div class="col-sm-9">
                            <select v-model="sortSearch" class="form-control">
                                <option v-for="sort in sorts" v-bind:value="sort.value">
                                    @{{ sort.text }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="row" v-for="item in products">
            <div class="col-xs-2">
                <a>
                    <img class="thumbnail col-xs-12" v-bind:src="item.thumbUrl">
                </a>
            </div>
            <div class="col-xs-10">
                <h4>
                    <a target="_blank" v-bind:href="item.sUrl">
                    <span>@{{item.caption}}</span>
                    </a>
                </h4>
                
                <h5>Price: <span>@{{item.price}}</span></h5>

                <br>
                <div class="btn-group btn-group-justified">
                    
                    <div>
                        <a class="btn btn-primary" role="button" title="Save" @click="addFavoriteItem(item.thumbUrl, item.sUrl)"><i class="fa fa-heart"></i></a>

                        <a class="btn btn-info" v-bind:href="item.sUrl" target="_blank"><i class="fa fa-external-link"></i> View Item</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn-block btn btn-info col-xs-12" v-if="seen" @click="loadMore()">Load More</a>
</div>
</div>
</etsy>
@endsection