@extends('home')
@section('cont')
<tee-spring-trends :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">SunFrog Best Sellers</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-1">
                    <label for="category">
                        Category:
                    </label>
                </div>
                <div class="col-md-6">
                    <select name="category" class="form-control" v-model="category" @change="getDataByCategory()" id="category">
                        <option v-for="item in categories" :value="item.value">@{{ item.name }}</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <div class="col-md-4" v-for="item in tshirtProducts">
            <div class="panel panel-default" style="min-height: 350px">
                <div class="panel-body">
                    <img class="thumbnail col-xs-12" id="image_0" v-bind:src="item.thumb" style="max-height: 215px">
                    <h4>
                        <a target="_blank" id="url_0" v-bind:href="item.DetailPageURL">
                        <span id="caption_0">@{{ item.title ? item.title.replace(/_/g, " ").substring(0,35) : ''}}</span>
                        </a>
                    </h4>
                    <div class="btn-group btn-group-justified">
                        <div>
                            <a class="btn btn-primary" role="button" title="Save" @click="addFavoriteItem( item.thumb, item.link)"><i class="fa fa-heart"></i></a>

                            <a class="btn btn-info" v-bind:href="item.link" target="_blank"><i class="fa fa-external-link"></i> View Item</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</tee-spring-trends>
@endsection