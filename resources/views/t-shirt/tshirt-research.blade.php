@extends('home')
@section('cont')
<tshirt-research :user="user" inline-template>
<div class="row">
<div class="panel panel-default col-md-12" style="padding: 0; margin-left: 15px">
    <div class="panel-heading">T-Shirt Research</div>
    <div class="panel-body">
        <form class="form-horizontal p-b-none" role="form" v-on:submit.prevent="search">
            <!-- Search Field -->
            <div class="form-group">
                <div class="col-md-9">
                    <input type="text" id="kiosk-users-search" class="form-control" v-model="searchResults">
                </div>
                <div class="col-md-3" style="padding-left: 0">
                    <a @click="search()" class="btn btn-lg btn-info" title="Search"><i class="fa fa-search"></i></a>
                    <a @click="clearSearch()" class="btn btn-lg btn-rs" data-toggle="tooltip" title="Reset"><i class="fa fa-reply"></i></a>
                    <a @click="addSearchKey()" title="Save" class="btn btn-lg btn-rs"><i class="fa fa-heart"></i></a>
                </div>
            </div>
        </form>
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li :class="[tabId == 1 ? 'active' : '']" @click="changeShop(1, 'images')" style="cursor: pointer">
                <a>Images</a>
            </li>
            <li :class="[tabId == 2 ? 'active' : '']" @click="changeShop(2, 'skreened')" style="cursor: pointer">
                <a>Skreened</a>
            </li>
            <li :class="[tabId == 3 ? 'active' : '']" @click="changeShop(3, 'cafepress')" style="cursor: pointer">
                <a>Cafepress</a>
            </li>
            <li :class="[tabId == 4 ? 'active' : '']" @click="changeShop(4, 'zazzle')" style="cursor: pointer">
                <a>Zazzle</a>
            </li>
            <li :class="[tabId == 5 ? 'active' : '']" @click="changeShop(5, 'sunfrog')" style="cursor: pointer">
                <a>SunFrog Shirts</a>
            </li>
            <li :class="[tabId == 6 ? 'active' : '']" @click="changeShop(6, 'teespring')" style="cursor: pointer">
                <a>Tee Spring</a>
            </li>
            <li><a :href="pinterestUrl" target="blank">
                Pinterest</a>
            </li>
        </ul>
        <br>
        <div class="col-md-4" v-for="item in tshirtProducts">
            <div class="panel panel-default" style="min-height: 350px">
                <div class="panel-body">
                    <img class="thumbnail col-xs-12" id="image_0" v-bind:src="item.thumb" style="max-height: 215px">
                    <h4>
                        <a target="_blank" id="url_0" v-bind:href="item.DetailPageURL">
                        <span id="caption_0">@{{ item.title ? item.title.replace(/_/g, " ").substring(0,35) : ''}}</span>
                        </a>
                    </h4>
                    <div class="btn-group btn-group-justified">
                        <div>
                            <a class="btn btn-primary" role="button" title="Save" @click="addFavoriteItem( item.thumb, item.link)"><i class="fa fa-heart"></i></a>

                            <a class="btn btn-info" v-bind:href="item.link" target="_blank"><i class="fa fa-external-link"></i> View Item</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn-block btn btn-info col-xs-12" v-if="seen" @click="loadTshirtMore()">Load More</a>
    </div>
</div>
</div>
</tshirt-research>
@endsection