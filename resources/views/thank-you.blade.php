@extends('spark::layouts.app')

@section('content')
<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				Thank You
			</div>
			<div class="panel-body">
				Thank you for subscription
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
@endsection