<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');
Route::get('/home', 'HomeController@show');
Route::post('/contact-us', 'ContactUsController@main');

### ShopsController ###
Route::get('/ebay', 'ShopsController@getEbay');
Route::get('/ebay-search/{query?}/{categoryID?}/{sort?}/{order?}/{page?}', 'ShopsController@getEbaySearch');
Route::get('/ali-express', 'ShopsController@getAliExpress');
Route::get('/ali-express-search/{query}/{category}/{sort}/{page?}', 'ShopsController@getAliExpressSearch');
Route::get('/amazon', 'ShopsController@getAmazon');
Route::get('/amazon-search/{query}/{page}', 'ShopsController@getAmazonSearch');
Route::get('/wanelo', 'ShopsController@getWanelo');
Route::get('/wanelo-search/{category?}/{sort?}/{query?}/{page?}', 'ShopsController@getWaneloSearch');
Route::get('/etsy', 'ShopsController@getEtsy');
Route::get('/etsy-search/{category?}/{sort?}/{query?}/{page?}', 'ShopsController@getEtsySearch');

### FavoritesController ###
Route::get('/favorites/search-key/{keyWord}/{shopId}', 'FavoritesController@getSearchKey');
Route::get('/favorites/delete-search-key/{id}', 'FavoritesController@deleteSearchKey');
Route::get('/favorites/delete-favorite-item/{id}', 'FavoritesController@getDeleteFavoriteItem');
Route::get('/favorites', 'FavoritesController@getFavorites');
Route::get('/favorite-items', 'FavoritesController@getFavoriteItems');
Route::post('/add-favorite-items', 'FavoritesController@postAddFavoriteItem');
Route::get('/favorites/search-keys', 'FavoritesController@getSearchKeys');

### AdvertisingController ###
Route::get('/fb-videos', 'AdvertisingController@getFbVideos');
Route::get('/fb-videos-search/{query?}', 'AdvertisingController@getFbVideosSearch');
Route::post('/fb-top-search/', 'AdvertisingController@getFbTopSearch');
Route::get('/fb-ads', 'AdvertisingController@getFbAds');


Route::get('/hot-youtube', 'AdvertisingController@getHotYoutube');
Route::get('youtube-video-search/{min_views?}/{common_vid?}/{query?}/{page?}', 'AdvertisingController@getYoutubeVideoSearch');
Route::get('youtube-most-popular', 'AdvertisingController@getYoutubeMostPopular');
Route::get('youtube-most-popular-data', 'AdvertisingController@getYoutubeMostPopularData');

Route::get('/hot-vimeo', 'AdvertisingController@getHotVimeo');
Route::get('vimeo-video-search/{query?}/{page?}', 'AdvertisingController@getVimeoVideoSearch');

Route::get('/hot-daily-motion', 'AdvertisingController@getHotDaily');
Route::get('/daily-motion-video-search/{query?}/{page?}', 'AdvertisingController@getDailyMotionVideoSearch');

### TshirtController ###
Route::get('t-shirt-research', 'TshirtController@getTshirtResearch');
Route::get('t-shirt-research/skreened/{query?}/{page?}', 'TshirtController@getSkreened');
Route::get('t-shirt-research/zazzle/{query?}/{page?}', 'TshirtController@getZazzle');
Route::get('t-shirt-research/cafepress/{query?}/{page?}', 'TshirtController@getCafepress');
Route::get('t-shirt-research/sunfrog/{query?}/{page?}', 'TshirtController@getSunFrog');
Route::get('t-shirt-research/teespring/{query?}/{page?}', 'TshirtController@getTeeSpring');
Route::get('t-shirt-research/images/{query?}/{page?}', 'TshirtController@getImages');

Route::get('t-shirt-research/sunfrog-best', 'TshirtController@getSunFrogBestPage');
Route::get('t-shirt-research/sunfrog-best-category/{category?}', 'TshirtController@getSunFrogBest');

Route::get('t-shirt-research/tee-spring-trends', 'TshirtController@getTeeSpringTrends');

Route::get('t-shirt-research/best-tee-spring-trends/{category?}', 'TshirtController@getBestTeeSpringTrends');

### ShopifyController ###
Route::get('/shopify-best-sellers', 'ShopifyController@getShopifyBestSellers');
Route::post('/shopify-best-sellers-search', 'ShopifyController@getShopifyBestSellersSearch');

### JVZoo ###
Route::post('/jvzoo-ipn', 'PlanController@addSubscription');
Route::delete('/cancel-subscription', 'PlanController@cancellSubscription');

Route::get('/thank-you', function ()
{
	return view('thank-you');
});