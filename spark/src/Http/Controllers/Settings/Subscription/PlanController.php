<?php

namespace Laravel\Spark\Http\Controllers\Settings\Subscription;

use Auth;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;
use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Interactions\Subscribe;
use Laravel\Spark\Events\Subscription\SubscriptionUpdated;
use Laravel\Spark\Events\Subscription\SubscriptionCancelled;
use Laravel\Spark\Http\Requests\Settings\Subscription\UpdateSubscriptionRequest;
use Laravel\Spark\Contracts\Http\Requests\Settings\Subscription\CreateSubscriptionRequest;

use Laravel\Spark\User as SparkUser;

class PlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create the subscription for the user.
     *
     * @param  CreateSubscriptionRequest  $request
     * @return Response
     */
    public function store(CreateSubscriptionRequest $request)
    {
        $plan = Spark::plans()->where('id', $request->plan)->first();

        Spark::interact(Subscribe::class, [
            $request->user(), $plan, false, $request->all()
        ]);
    }

    /**
     * Update the subscription for the user.
     *
     * @param  \Laravel\Spark\Http\Requests\Settings\Subscription\UpdateSubscriptionRequest  $request
     * @return Response
     */
    public function update(UpdateSubscriptionRequest $request)
    {
        $plan = Spark::plans()->where('id', $request->plan)->first();

        // This method is used both for updating subscriptions and for resuming cancelled
        // subscriptions that are still within their grace periods as this swap method
        // will be used for either of these situations without causing any problems.
        if ($plan->price === 0) {
            return $this->destroy($request);
        } else {
            $subscription = $request->user()->subscription();

            if (Spark::prorates()) {
                $subscription->swap($request->plan);
            } else {
                $subscription->noProrate()->swap($request->plan);
            }
        }

        event(new SubscriptionUpdated(
            $request->user()->fresh()
        ));
    }

    /**
     * Cancel the user's subscription.
     *
     * @param  Request  $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $request->user()->subscription()->cancel();

        event(new SubscriptionCancelled($request->user()->fresh()));
    }

    // /**
    //  * Create subscription jVzoon .
    //  *
    //  * @param  Request  $request
    //  * @return Response
    //  */
    // public function addSubscription(Request $request)
    // {
    //     $requestData = $request->all();
    //     $user = SparkUser::where('id', 1)->first();
    //     // $userId = Auth::user()->id;
    //     $stripeId = $requestData['caffitid'];
    //     $stripePlan = $requestData['cprodtitle'];
    //     $subscription = new Subscription;
    //     // $data = [
    //     //     'user_id' => 1,
    //     //     'stripe_id' => $stripeId,
    //     //     'stripe_plan' => $stripePlan,
    //     //     'quantity' => 1,
    //     //     'name' => 'default'
    //     // ];

    //     $data = [
    //         "stripe_token" => "tok_19rpUdKrokuRgvBcIrHqrR6h",
    //         "plan" => $stripePlan,
    //         "coupon" => null,
    //         "address" => null,
    //         "address_line_2" => null,
    //         "city" => null,
    //         "state" => null,
    //         "zip" => "0069",
    //         "country" => "US",
    //         "vat_id" => null,
    //         "errors" => [
    //         "errors" => [],
    //         ],
    //         "busy" => true,
    //         "successful" => false
    //     ];

    //     $plan = Spark::plans()->where('id', 'monthly-pro')->first();

    //     $aaa = Spark::interact(Subscribe::class, [
    //         $user, $plan, false, $data
    //     ]);

    //     dd($aaa);
    // } 
}
